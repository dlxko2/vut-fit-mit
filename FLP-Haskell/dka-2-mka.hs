{-|
Module      : dka-2-mka
Description : Minimize the deterministic finite machine
Copyright   : (c) Martin Pavelka, 2017
Login       : xpavel27
Maintainer  : xpavel27@stud.fit.vutbr.cz
Stability   : production
-}

import System.IO
import System.Environment
import System.Exit
import Data.Char
import Data.List
import Data.List.Split
import Text.Printf
import Text.ParserCombinators.Parsec hiding ((<|>), many)
import Control.Applicative
import Control.Monad

{-| The structure for the transitions -}
data Transition = TRN
  {
    from   :: String,
    symbol :: Char,
    to     :: String,
    sink   :: Bool
  } deriving (Eq)

{-| The structure for the finite machine -}
data FiniteMachine = FSM
  { states   :: [String]
  , alphabet :: String
  , start    :: String
  , end      :: [String]
  , trans    :: [Transition]
  } deriving (Eq)

{-| Deriving the show for finite machine -}
instance Show FiniteMachine where
  show (FSM q a s f t) =
    "States: "        ++ show q ++ "\n" ++
    "Alphabet: "      ++ show a ++ "\n" ++
    "Start: "         ++ show s ++ "\n" ++
    "Finish: "        ++ show f ++ "\n" ++
    "Transitions: \n" ++ show t

{-| Deriving the show for transitions -}
instance Show Transition where
  show (TRN f s t x) =
    "("   ++ show f ++
    ")-"  ++ show s ++
    "->(" ++ show t ++
    ")["  ++ show x ++ "]\n"

{-| Sorting using the state -}
sortForTransitionByState :: Transition -> Transition -> Ordering
sortForTransitionByState x y
  | from x > from y = GT
  | otherwise = LT

{-| Sorting using the symbol -}
sortForTransitionBySymbol :: Transition -> Transition -> Ordering
sortForTransitionBySymbol x y
  | (from x >= from y) && symbol x > symbol y = GT
  | otherwise = LT

{-| Sorting for the minimization-}
sortForMinimize :: Ord a => [a] -> [a] -> Ordering
sortForMinimize x y
  | tail x < tail y = GT
  | otherwise = LT

{-| Produce the final output for the transitions -}
printTRN :: [Transition] -> IO [()]
printTRN = mapM processTransition
processTransition transition = putStrLn (printf "%s,%c,%s" (from transition) (symbol transition) (to transition))

{-| Produce the final output for the finite machine -}
printFSM :: FiniteMachine -> IO [()]
printFSM fsm = do
  putStrLn (intercalate "," (map printf (states fsm)))
  printf "%s\n" (start fsm)
  putStrLn (intercalate "," (map printf (end fsm)))
  printTRN (trans fsm)

{-| Check the states from the input using the parser -}
checkStates :: Parser [[String]]
checkStates = do
    states <- many1(many1 (letter <|> digit) `sepBy1` char ',' )
    char '\n'
    return states

{-| Check the start state using the parser -}
checkStart :: Parser String
checkStart = do
    start <- many1 (letter <|> digit)
    char '\n'
    return start

{-| Check the finishing states using the parser -}
checkFinishes :: Parser [[String]]
checkFinishes = do
    finishes <- many1(many1 (letter <|> digit) `sepBy1` char ',' )
    char '\n'
    return finishes

{-| Check the one transition using the parser -}
checkTransition :: Parser (String, Char, String)
checkTransition = do
    from <- many1 (letter <|> digit)
    char ','
    symbol <- letter
    char ','
    to <- many1 (letter <|> digit)
    char '\n'
    return (from,symbol, to)

{-| Check all the transitions using the parser -}
checkTransitions :: Parser [(String, Char, String)]
checkTransitions = many1 checkTransition

{-| Check the all requested parts -}
fsmParser :: Parser Bool
fsmParser = do
    checkStates
    checkStart
    checkFinishes
    checkTransitions
    return True

{-| Check the result from the parser -}
checkFSM :: Parser Bool -> String -> Bool
checkFSM parser input = case parse parser "" input of
    Left _ -> True
    Right True -> False

{-| The main function to minimize the finite machine
    @return the final output for the finite machine -}
main :: IO ()
main = do
  -- Get the arguments using the getArgs and parse them into structure
  args <- getArgs
  let (useBaseResult, useInputFile, useMonadicParser) = parseArguments args
  -- Check the input with monadic parser
  inputFSM <- useInputFile
  when useMonadicParser $ when (checkFSM fsmParser inputFSM) $ error "The wrong input"
  -- Get the internal representation of the finite machine
  baseFSM <- getFiniteMachine useInputFile
  -- If we want just the internal structure representation print it
  if useBaseResult then do
      printFSM baseFSM
      return ()
  -- In other way we create minimized FSM
  else do
    -- Well defined
    let wellDefinedFSM = produceWellDefinedFSM baseFSM
    -- Reduced
    let firstIteration = getFirstIteration wellDefinedFSM
    classList <- classIterator firstIteration True wellDefinedFSM
    let convertedFSM = convertFSM wellDefinedFSM classList
    -- Minimized
    updated <- minimizeFSM convertedFSM (end convertedFSM) True
    let minimalFSM = updateMinimizedFSM convertedFSM updated
    -- Final
    let finalFSM = createFinalFSM minimalFSM
    printFSM finalFSM
    return ()

{-| Parse the arguments into the structure
    @param String - the input arguments
    @return Bool - if just the internal FM representation
    @return IO String - the stdin input or read file -}
parseArguments :: [String] -> (Bool, IO String, Bool)
parseArguments []  = error "The arguments are required!"
parseArguments [x]
  | x == "-i" = (True, getContents, True)
  | x == "-t" = (False, getContents, True)
  | otherwise = error "The program received the unexpected argument!"
parseArguments [x,y]
  | x == "-i" = (True, readFile y, True)
  | x == "-t" = (False, readFile y, True)
  | otherwise = error "The program received the unexpected argument!"
parseArguments [x,y,z]
  | x == "-i" && z == "-x" = (True, readFile y, False)
  | x == "-t" && z == "-x" = (False, readFile y, False)
  | otherwise = error "The program received the unexpected argument!"
parseArguments _ = error "The number of arguments is wrong!"

{-| Prepare the process of creating the base finite machine representation
    @see http://zvon.org/other/haskell/Outputprelude/lines_f.html
    @param input - the input file or stdin from the arguments
    @return fsm - the output as internal structure of finite machine -}
getFiniteMachine :: Monad m => m String -> m FiniteMachine
getFiniteMachine input = do
    content <- input;
    let linesOfInput = lines content
    let usefulLines = getNotEmptyLines linesOfInput
    let fsm = processInputLines usefulLines
    return fsm;

{-| Function to filter just useful lines from the input
    @param linesArray - the array of lines from the input
    @return the filtrated array of input lines without empty ones -}
getNotEmptyLines :: [String] -> [String]
getNotEmptyLines = filter (/= "")

{-| Function to iterate through the class lists - finish when the lists is the same in two iterations
    @see http://stackoverflow.com/questions/17719620/while-loop-in-haskell-with-a-condition
    @param classForIteration - the class list to be processed
    @param finalPredicate - the predicate if finish the loop
    @param fsm - the well defined finite state machine
    @return classForIteration - the final class list -}
classIterator :: Monad m => [[String]] -> Bool -> FiniteMachine -> m [[String]]
classIterator classForIteration finalPredicate fsm
  | finalPredicate = do
    let minimizeStruct = createMinimalizeStructure classForIteration fsm
    let minimizeClasses = getMinimizeClasses minimizeStruct
    classIterator minimizeClasses (compareIterations classForIteration minimizeClasses) fsm
  | otherwise = return classForIteration

{-| Function which performs the check if the lists are the same
     @param firstClass - the previous list iteration
     @param secondClass - the actual list iteration
     @return Bool - the result predicate value -}
compareIterations :: Eq a => a -> a -> Bool
compareIterations firstClass secondClass = firstClass /= secondClass

{-| Process the input lines and produce the internal structure of finite machine
    @param (states:start:end:actions) - the input array divided into the sections
    @return FiniteMachine - the finite machine internal structure
-}
processInputLines :: [String] -> FiniteMachine
processInputLines (states:start:end:actions) = FSM getStates getAlphabet getStart getFinal getRules
  where
    -- Get trivial inputs
    getStates = splitOn "," states
    getStart = start
    getFinal = splitOn "," end
    -- Get the transitions
    getRules = sortBy sortForTransitionBySymbol sortRules
    sortRules = sortBy sortForTransitionByState (map splitRules actions)
    splitRules transitionItem = getTransElements (splitOn "," transitionItem)
    getTransElements [q1,[sym],q2] = TRN q1 sym q2 False
    getTransElements _ = error "Bad transition in the input!"
    -- Get alphabet
    getAlphabet = nub (map splitAlphabet actions)
    splitAlphabet splitItems = getElements (splitOn "," splitItems)
    getElements [q1,[sym],q2] = sym

{-| Try to make well defined FSM and check if already is well defined
    @param fsm - FSM which can be well defined or not
    @return FiniteMachine - FSM which is well defined
-}
produceWellDefinedFSM :: FiniteMachine -> FiniteMachine
produceWellDefinedFSM fsm =
  if length (states fsm) * length (alphabet fsm) == length (trans fsm) then fsm
  else createWellDefinedFSM fsm

{-| If the finite machine is not well defined use the SINK state to make it
    @param FiniteMachine - FSM which is not well defined
    @return FiniteMachine - FSM which is well defined using SINK
-}
createWellDefinedFSM :: FiniteMachine -> FiniteMachine
createWellDefinedFSM fsm = fsm { trans = getTrans, states =  states fsm ++ ["SINK"] }
  where
    -- Sort the output by state and symbol
    getTrans = sortBy sortForTransitionBySymbol sortTrans
    sortTrans = sortBy sortForTransitionByState (trans fsm ++ sinks)
    -- Get the sinks states as transitions
    sinks = map getSinks difference
    getSinks (x,y) = TRN x y "SINK" True
    -- Calculate the missing transitions
    difference = all \\ defined
    -- Get already defined transitions as lists
    defined = map getDefined (trans fsm)
    getDefined transition = (from transition, symbol transition)
    -- Get all available states from generator (states, symbols)
    all = [(from, symbol) | from <- getStates, symbol <- alphabet fsm]
    getStates = states fsm ++ ["SINK"]

{-| Function to get the first iteration like final states | other states
    @param fsm - the finite machine internal representation
    @return the first list of categories -}
getFirstIteration :: FiniteMachine -> [[String]]
getFirstIteration fsm = [end fsm, states fsm \\ end fsm]

{-| Get the destination state for the start/symbol in transition
    @param transitions - the list of transitions from the finite machine
    @param startState - the starting state from the finite machine
    @param sym - the symbol used to enter the another state
    @return String - the target state
-}
findDestinationState :: [Transition] -> String -> Char -> String
findDestinationState transitions startState sym = to process
  where
    process = head useFilter
    useFilter = filter (\n -> from n == startState && symbol n == sym) transitions

{-| Get the class number for the input element
    @param listOfClasses - the list of all classes where to find
    @param element - the element to get the category representation
    @return String - the output class as the string
-}
getClassNumber :: Eq t => [[t]] -> t -> String
getClassNumber listOfClasses element = show (sum process)
  where
    process = mapInd findInClass listOfClasses
    findInClass classElem index =
      case elemIndex element classElem of
        Just value -> index + 1
        Nothing    -> 0

{-| The same like the map function but this will pass the index
    @see http://stackoverflow.com/questions/16191824/index-of-element-in-list-in-haskell
    @param function - the function to be called for each element
    @param list - the array to be parsed by the mapInd -}
mapInd :: (a -> Int -> b) -> [a] -> [b]
mapInd function list = zipWith function list [0..]

{-| Get the minimize structure as [state,classIfAlpha1,classIfAlphaN]
    @param statesList - the list of states from the finite machine
    @param fsm - the finite machine internal representation
    @return the minimize structure
-}
createMinimalizeStructure :: [[String]] -> FiniteMachine -> [[[String]]]
createMinimalizeStructure statesList fsm = stepClasses
  where
    stepClasses = map stepThroughClasses statesList
    stepThroughClasses = map createFinalElem
    createFinalElem element = element : map (processSymbols element) (alphabet fsm)
    processSymbols elem sym = getClassNumber statesList (findDestinationState (trans fsm) elem sym)

{-| Get the groups of states from the minimized structure
    @param minimizeList - the minimized structure like [state,classIfAlpha1,classIfAlphaN]
    @return the grouped states in array
-}
getMinimizeClasses :: Ord a => [[[a]]] -> [[a]]
getMinimizeClasses minimizeList = concat getJustStates
  where
    -- Get just the states
    getJustStates = map enterMinimize groupTheElements
    enterMinimize = map enterMinimizeElements
    enterMinimizeElements = map parseMinimizeElem
    parseMinimizeElem = head
    -- Group the transitions which can be minimized
    groupTheElements = map enterMinimizeStructure minimizeList
    enterMinimizeStructure minimize = groupBy (\x y -> tail x == tail y) (sortBy sortForMinimize minimize)

{-| Convert the finite machine structure with new minimized states
    @param fsm - the previous well defined or minimized finite machine
    @param classList - the list of classes from the getMinimizeClasses
    @return FiniteMachine - the updated finite machine after minimization
-}
convertFSM :: FiniteMachine -> [[String]] -> FiniteMachine
convertFSM fsm classList = fsm { trans = getTrans, states = getStates, end = getFinishes, start = getStart}
  where
   -- Update the start state
   getStart = nub (getClassNumber classList (start fsm))
   -- Update the finishes
   getFinishes = nub (map procFinishes (end fsm))
   procFinishes = getClassNumber classList
   -- Update the states
   getStates =  sort (nub (map procStates (states fsm)))
   procStates = getClassNumber classList
   -- Update the transitions
   getTrans = filter (not . sink) sortTransitions
   sortTransitions = sortBy sortForTransitionByState transitions
   transitions =  nub (map procTrans (trans fsm))
   procTrans inputTrans = TRN
     (getClassNumber classList (from inputTrans))
     (symbol inputTrans) (getClassNumber classList (to inputTrans)) (sink inputTrans)

{-| Function to find the states which can end in the end state
    @param fsm - the finite machine which is reduced
    @param states - the actual states array
    @return - the updated states array with new states which can be final
-}
getFinalStates :: FiniteMachine -> [String] -> [String]
getFinalStates fsm states = map from (filter (\transition -> to transition `elem` states) (trans fsm))

{-| Function to minimize the FSM and remove redundant states
    @param fsm - the finite machine which is reduced
    @param states - the actual states array
    @param predicate - if the new array is the same as previous
    @return - states without the redundant ones
-}
minimizeFSM :: Monad m => FiniteMachine -> [String] -> Bool -> m [String]
minimizeFSM fsm states predicate
  | predicate =  do
    let newStates = nub $ states ++ getFinalStates fsm states
    minimizeFSM fsm newStates (compareUnused states newStates)
  | otherwise = return states

{-| Function to compare the two states arrays
    @param firstStates - the first array to be compared
    @param secondStates - the second array to be compared
    @return Bool - if the arrays are the same or not
-}
compareUnused :: Eq a => a -> a -> Bool
compareUnused firstStates secondStates = firstStates /= secondStates

{-| Function to update the minimized FSM structure
    @param fsm - the reduced FSM
    @param finalStates - just the states which can finish
    @return FiniteMachine - the minimized Finite Machine
-}
updateMinimizedFSM :: FiniteMachine -> [String] -> FiniteMachine
updateMinimizedFSM fsm finalStates = fsm { trans = filter (\x -> to x `elem` finalStates) (trans fsm) }

{-| Function to create the final format of FSM
    @param fsm - the minimized FSM
    @return FiniteMachine - the final FSM
-}
createFinalFSM :: FiniteMachine -> FiniteMachine
createFinalFSM fsm = fsm {states = getStates}
  where
    getFinishes = nub $ intersect (end fsm) (map from (trans fsm) ++ map to (trans fsm))
    getStates =  nub $ map from (trans fsm) ++ map to (trans fsm)
