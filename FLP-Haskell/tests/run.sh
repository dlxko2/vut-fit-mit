#!/bin/bash

#Script      : run.sh
#Description : Start the tests
#Modified by : Martin Pavelka, 2017
#Login       : xpavel27
#Maintainer  : xpavel27@stud.fit.vutbr.cz
#Stability   : production
#Source      : https://github.com/vokracko/FLP-DKA-2-MKA-test


function compareFsm
{
	input=$1
	name=$(basename "$input")

	../dka-2-mka -i $input > "./res/$name"
    ./fstcompile.sh "./res/$name" "./resCompiled/$name.fst"
   	fstisomorphic "./resCompiled/$name.fst" "./compiledPlain/$name.fst"

   	if [ $? -eq 0 ] ; then
   		st="\033[1;32mPASS\033[0m"
   	else
   	    st="\033[1;31mFAIL\033[0m"
    fi

    ../dka-2-mka -t $input -x > "./resMinimal/$name"
	./fstcompile.sh "./resMinimal/$name" "./resMinimalCompiled/$name.fst"
	fstisomorphic "./resMinimalCompiled/$name.fst" "./compiledPlainMinimal/$name.fst"

	if [ $? -eq 0 ] ; then
		nd="\033[1;32mPASS\033[0m"
	else
		nd="\033[1;31mFAIL\033[0m"
	fi

	../dka-2-mka -t $input > "./resMinimal/$name"
	./fstcompile.sh "./resMinimal/$name" "./resMinimalCompiled/$name.fst"
	fstisomorphic "./resMinimalCompiled/$name.fst" "./compiledPlainMinimal/$name.fst"

	if [ $? -eq 0 ] ; then
		th="\033[1;32mPASS\033[0m"
	else
		th="\033[1;31mFAIL\033[0m"
	fi

    echo -e "Processed: -i $st -t -x $nd -t $th \t$input"
}

[[ $# -eq 1 ]] && {
	checkFsm $1
} || {
	for input in $(ls ./plain/*) ; do
		compareFsm $input
	done
}
