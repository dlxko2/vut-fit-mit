#!/bin/bash

#Script      : prepare.sh
#Description : Start the tests
#Modified by : Martin Pavelka, 2017
#Login       : xpavel27
#Maintainer  : xpavel27@stud.fit.vutbr.cz
#Stability   : production
#Source      : https://github.com/vokracko/FLP-DKA-2-MKA-test

find ./plain -type f | while read file ; do
    name=$(basename "$file")
	echo $file "./compiledPlain"
	./fstcompile.sh $file "./compiledPlain/$name.fst"
done

find ./plain-minimal -type f | while read file ; do
    name=$(basename "$file")
	echo $file "./compiledPlainMinimal" $name
	./fstcompile.sh $file "./compiledPlainMinimal/$name.fst"
done
