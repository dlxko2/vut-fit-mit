/**
 * POS Project - Threads
 * @name 1. projekt - ticket algoritmus
 * @see https://wis.fit.vutbr.cz/FIT/st/course-sl.php?id=628059&item=62544
 * @author Martin Pavelka
 * @date 19.02.2017
 */

#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <time.h>

/* Global Vatiables */
int ticket;
int steps;
int actualTicket;
pthread_t *thread;

/* Mutex initializations */
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexTicket = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

/* Constants */
#define ERROR 1
#define SUCCESS 0
#define MAX_WAIT_TIME 500000000L
#define INT2VOIDP(i) (void*)(uintptr_t)(i)

/**
 * Function to get the ticket number
 * We also lock this process with mutex
 * @return the id of the ticket
 */
int getticket(void) {

    /* Initialize the critical section */
    int storeTicket = 0;
    pthread_mutex_lock(&mutexTicket);

    /* Increment the ticket and save it */
    ++ticket;
    storeTicket = ticket;

    /* Unlock the critical and return the ticket */
    pthread_mutex_unlock(&mutexTicket);
    return storeTicket;
}

/**
 * Function to enter the critical section
 * @see https://www.fit.vutbr.cz/study/courses/POS/private/slides/pos15-07.pdf
 * @page 108
 * @param aenter - the ticket number
 */
void await(int aenter) {

    pthread_mutex_lock(&mutex);
    while (aenter != actualTicket) {
       pthread_cond_wait(&cond, &mutex);
    }
}

/**
 * Function to leave the critical section
 * @see https://www.fit.vutbr.cz/study/courses/POS/private/slides/pos15-07.pdf
 * @page 109
 */
void advance(void) {

    actualTicket++;
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mutex);
}

/**
 * Function to be handled by the thread
 * @see https://users.pja.edu.pl/~jms/qnx/help/watcom/clibref/qnx/clock_gettime.html
 * @param arg - the thread id
 * @return - the void
 */
void *threadRoutine(void *arg) {

    /* Initialize the local variables */
    int localTicket = 0;
    int threadNumber = (intptr_t)arg;

    while ((localTicket = getticket()) <= steps) {

        /* Get the time in nanoseconds */
        struct timespec time, end;
        unsigned int seed;
        unsigned int random;

        /* Get the random value using the time and tread number */
        seed = (unsigned int) (getppid() + threadNumber);
        random = (unsigned long)rand_r(&seed);
        time.tv_sec = 0;
        time.tv_nsec = random % MAX_WAIT_TIME;

        /* Wait and try to enter critical section */
        nanosleep(&time, NULL);
        await(localTicket);

        /* Write to the output */
        printf("(%d) %d\n", localTicket, threadNumber);

        /* Leave the critical section */
        advance();

        /* Get another random time */
        seed = (unsigned int) (getppid() + threadNumber);
        random = (unsigned long)rand_r(&seed);
        end.tv_sec = 0;
        end.tv_nsec = random % 500000000;

        /* Sleep for that time */
        nanosleep(&end, NULL);
    }

    pthread_exit(0);
}

/**
 * Function to print the help page
 */
void help() {

    printf("#Author: Martin Pavelka\n");
    printf("#Modified: 19.02.2017\n");
    printf("#Param1: N - number of threads\n");
    printf("#Param2: M - number of critical section enters\n");
}

/**
 * Function to check if the text is number
 * @param number - the text
 * @return - if it is a number
 */
bool isNumber(char number[]) {

    /* Variables for C90 */
    int i;

    /* Check the minus */
    if (number[0] == '-') return false;

    /* Check another digits */
    for (i = 0; number[i] != 0; i++)
        if (!isdigit(number[i]))
            return false;

    /* This is the success */
    return true;
}

/**
 * Function to destroy the mutexes
 */
void destroyMutexes() {

    pthread_mutex_destroy(&mutex);
    pthread_mutex_destroy(&mutexTicket);
    pthread_cond_destroy(&cond);
}

/**
 * Handle the signals
 * @param signal - the signal number
 */
void signalHandler(int signal) {
    if (signal == SIGINT || signal == SIGTERM) {
        printf("Cancel signal received - quiting...\n");
        destroyMutexes();
        free(thread);
        exit(1);
    }
}

/**
 * Function to create the signal handlers
 * @return the success of the operations
 */
bool createSignals(){

    if (signal(SIGINT, signalHandler) == SIG_ERR)
        return false;
    if (signal(SIGTERM, signalHandler) == SIG_ERR)
        return false;
    return true;
}

/**
 * The main function of the program
 * @param argc - number of the arguments
 * @param argv - the array of the arguments
 * @return - the program exit code
 */
int main(int argc, char *argv[]) {

    /* Variables C90 */
    int threads;
    int i;
    int j;
    int res;
    pthread_attr_t attr;

    /* Check if we have two parameters */
    if (argc != 3 || !isNumber(argv[1]) || !isNumber(argv[2])) {
        help();
        return ERROR;
    }

    /* Create the signals */
    if (createSignals() == false) {
        perror("Problem while creating the signals");
        return ERROR;
    }

    /* Save the input parameters */
    threads = atoi(argv[1]);
    steps = atoi(argv[2]);
    ticket = -1;
    actualTicket = 0;

    /* Create the threads and signals */
    thread = malloc(sizeof(pthread_t) * threads);
    for (i = 0; i < threads; i++) {

        /* Initialize the arguments */
        if ((res = pthread_attr_init(&attr)) != 0) {
            fprintf(stderr, "Problem while creating attr in thread %d:  %d\n", i, res);
            return ERROR;
        }

        /* Set joinable attribute */
        if ((res = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE)) != 0) {
            fprintf(stderr, "Problem while setting attr in thread %d: %d\n", i, res);
            return ERROR;
        }

        /* Create the thread - using the macro int -> *void */
        if (pthread_create(&thread[i], &attr, threadRoutine, INT2VOIDP(i)) != 0) {
            perror("Can't create the threads");
            return 1;
        }
    }

    /* Wait until completed */
    for (j = 0; j<threads; j++) {
        if (pthread_join(thread[j], NULL) != 0) {
            perror("Can't join the threads");
            return ERROR;
        }
    }

    /* Destroy the mutexes */
    free(thread);
    destroyMutexes();
    return SUCCESS;
}