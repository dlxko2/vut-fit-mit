#!/bin/bash

#pocet cisel bud zadam nebo 10 :)
if [ $# -lt 1 ];then
    numbers=3;
else
    numbers=$1;
fi;

#preklad cpp zdrojaku
mpic++ --prefix /usr/local/share/OpenMPI -o esort es.cpp


#vyrobeni souboru s random cisly
dd if=/dev/random bs=1 count=$numbers of=numbers >& /dev/null

#spusteni
mpirun --prefix /usr/local/share/OpenMPI -np $(($numbers + 1)) esort

#uklid
rm -f esort numbers
