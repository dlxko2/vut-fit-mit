/**
 * Created By Martin Pavelka
 */

#include <mpi.h>
#include <iostream>
#include <fstream>
#include <stdint.h>

/** Defines **/
#define INPUT_FILE "numbers"
#define W_REGISTER 0
#define X_REGISTER 1
#define Y_REGISTER 2
#define Z_REGISTER 3
#define CONTROL_PROC 1
#define LAST_ELEM 1
#define TRACK_TIME 0
using namespace std;

/** Prototypes **/
void funcControlProcessor(int numProcessors, MPI_Status stat);
void funcOtherProcessor(MPI_Status stat, int numProcessors, int myID);
void printFirstLineSequence(int array[], int length);
void printOutputSequence(int array[], int length);
void pushTheZRegisters(int myID, int zReg, int processors, MPI_Status stat);
int processTheInputSignals(int processors, int myID, int yReg, int xReg, int cReg, MPI_Status stat);

/**
 * Main function of the program
 * @param argc - the arguments count
 * @param argv - the arguments data
 * @return int - the result
 */
int main(int argc, char *argv[]) {

    // Initialize the variables
    int myID;
    int numProcessors;

    // Initialize the MPI
    MPI_Status stat;
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcessors);
    MPI_Comm_rank(MPI_COMM_WORLD, &myID);

    // This is control processor
    if (myID == numProcessors-1) funcControlProcessor(numProcessors, stat);
    else funcOtherProcessor(stat, numProcessors, myID);

    // Finalize
    MPI_Finalize();
}

/**
 * Implements the functionality of other processors
 * @param stat - the MPI status
 * @param numProcessors - the number of processors
 * @param myID - the my process ID
 */
void funcOtherProcessor(MPI_Status stat, int numProcessors, int myID) {

    // Initialize the registers, C = 0
    int cReg = 0; int xReg = -1; int yReg = -1; int zReg = -1;

    // Get the X from the control
    MPI_Recv(&xReg, 1, MPI_INT, numProcessors-CONTROL_PROC, X_REGISTER, MPI_COMM_WORLD, &stat);

    // Process the inputs
    cReg = processTheInputSignals(numProcessors, myID, yReg, xReg,cReg, stat);

    // Update the Z registers
    MPI_Send(&xReg, 1, MPI_INT, cReg, Z_REGISTER, MPI_COMM_WORLD);
    MPI_Recv(&zReg, 1, MPI_INT, MPI_ANY_SOURCE, Z_REGISTER, MPI_COMM_WORLD, &stat);

    // Produce the output
    pushTheZRegisters(myID, zReg, numProcessors, stat);
}

/**
 * Function to process the input signals and get the right c registers
 * @param processors - the number of processors
 * @param myID - the my process ID
 * @param yReg - the Y register
 * @param xReg - the X register
 * @param cReg - the C register
 * @param stat - MPI status
 * @return - the updated C register
 */
int processTheInputSignals(int processors, int myID, int yReg, int xReg, int cReg, MPI_Status stat) {

    for (int i = 0; i < processors-CONTROL_PROC; i++) {

        // Get the Y from the previous
        if (myID != 0) MPI_Recv(&yReg, 1, MPI_INT, myID-1, Y_REGISTER, MPI_COMM_WORLD, &stat);
        else MPI_Recv(&yReg, 1, MPI_INT, processors-CONTROL_PROC, Y_REGISTER, MPI_COMM_WORLD, &stat);

        // Send the number to next processor Y register
        if (myID < processors-CONTROL_PROC-LAST_ELEM) MPI_Send(&yReg, 1, MPI_INT, myID+1, Y_REGISTER, MPI_COMM_WORLD);

        // Increase the C register
        if (xReg != -1 && yReg != -1)
            if (xReg > yReg || (xReg == yReg && i > myID)) cReg++;
    }
    return cReg;
}

/**
 * Function to push the Z register to produce the output
 * @param myID - my process ID
 * @param zReg - my Z register
 * @param processors - number of processors
 * @param stat - MPI status
 */
void pushTheZRegisters(int myID, int zReg, int processors, MPI_Status stat) {

    for (int j = 0; j < myID+1; ++j) {

        if (j != 0) MPI_Recv(&zReg, 1, MPI_INT, myID-1, W_REGISTER, MPI_COMM_WORLD, &stat);

        if (myID < processors-CONTROL_PROC-LAST_ELEM) MPI_Send(&zReg, 1, MPI_INT, myID+1, W_REGISTER, MPI_COMM_WORLD);
        else MPI_Send(&zReg, 1, MPI_INT, processors-CONTROL_PROC, W_REGISTER, MPI_COMM_WORLD);
    }
}

/**
 * Functionality of the control processor
 * @param numProcessors - the number of processors
 * @param stat - the MPI status
 */
void funcControlProcessor(int numProcessors, MPI_Status stat) {

    // Open the input file
    fstream fin;
    fin.open(INPUT_FILE, ios::in);

    // Initialize file variables
    int i = 0;
    int numOfElements = numProcessors - 1;
    int rawInput[numOfElements];

    // Get all numbers from the input
    while(fin.good()){
        if(!fin.good()) break;
        rawInput[i] = fin.get();
        i++;
    }

    // Close the file
    fin.close();

    // Print the first line
    printFirstLineSequence(rawInput, numOfElements);

    // Clock if time tracked
    struct timespec start, ende;
    if (TRACK_TIME) clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    // Get the output from the processors
    int sortOut[numOfElements];
    for (i = 1; i <= numOfElements; i++)
        MPI_Recv(&sortOut[numOfElements-i], 1, MPI_INT, numOfElements-CONTROL_PROC, W_REGISTER, MPI_COMM_WORLD, &stat);

    // Write the time output
    if (TRACK_TIME) {
        clock_gettime(CLOCK_MONOTONIC_RAW, &ende);
        uint64_t deltaS = (uint64_t) (ende.tv_sec - start.tv_sec);
        uint64_t deltaM = (uint64_t) ((ende.tv_nsec - start.tv_nsec) / 1000000);
        uint64_t deltaMC = (uint64_t) ((ende.tv_nsec - start.tv_nsec) / 1000);
        printf("Time: %llu seconds %llu miliseconds %llu microseconds \n", deltaS, deltaM, deltaMC);
    }

    // Print the result
    printOutputSequence(sortOut, numOfElements);
}

/**
 * Print the unique sequence of the numbers
 * @param array - the input array to be printed
 * @param length - the length of the input array
 * @complexity - O(n) seq
 */
void printFirstLineSequence(int array[], int length) {

    for (int j = 0; j < length; ++j) {

        printf("%d", array[j]);
        if (j < length-LAST_ELEM) printf(",");
        else printf("\n");

        MPI_Send(&array[j], 1, MPI_INT, 0, Y_REGISTER, MPI_COMM_WORLD);
        MPI_Send(&array[j], 1, MPI_INT, j, X_REGISTER, MPI_COMM_WORLD);
    }
}

/**
 * Print the output sequence on the stdout
 * @param array - the input array
 * @param length - the length of the sequence
 * @complexity - O(n) seq
 */
void printOutputSequence(int array[], int length) {

    for (int k = 0; k < length; ++k)
        printf("%d\n", array[k]);
}
