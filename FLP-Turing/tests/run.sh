#!/bin/bash

#Script      : run.sh
#Description : Start the tests
#Author      : (c) Martin Pavelka, 2017
#Login       : xpavel27
#Maintainer  : xpavel27@stud.fit.vutbr.cz
#Stability   : production

bold=$(tput bold)
normal=$(tput sgr0)


function compare
{
	input=$1
	name=$(basename "$input")

	../flp17-log < $input > "./outputs/$name"
    var=$(comm -3 ./outputs/$name ./refs/$name)

   	if [ -z "$var" ]
	then
    	nd="\033[1;32m${bold}PASS${normal}\033[0m"
	else
    	nd="\033[1;31m${bold}FAIL${normal}\033[0m"
	fi

	echo -e  "Processed $name: $nd"

}

function compareWrong
{
	input=$1
	name=$(basename "$input")

	../flp17-log < $input >> /dev/null 2>&1 
  
   	if [ $? == 0 ]
	then
		nd="\033[1;31m${bold}FAIL${normal}\033[0m"
	else
    	nd="\033[1;32m${bold}PASS${normal}\033[0m"
	fi

	echo -e "Processed $name: $nd"
}

rm -R -f ./outputs
mkdir outputs

echo -e "\033[0;35m${bold}Starting deterministic tests...${normal}\033[0m"
for input in $(ls ./inputs/*) ; do
		compare $input
done

echo -e "\033[0;35m${bold}Starting wrong inputs tests...${normal}\033[0m"
for input in $(ls ./wrongInputs/*) ; do
		compareWrong $input
done
