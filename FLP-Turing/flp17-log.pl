/** <turing> Turing machine

This is the implementation of the nondeterminist turing machine,
this file contains the stdin reading, input check, initialization
of TM, simulation using L,R,Update functions. Finaly printing the
all configurations in the standart output.

@author Martin Pavelka (xpavel27)
@email xpavel27@stud.fit.vutbr.cz
@date 30.04.2017

*/

% ===============================================================
% Input stdin functionality 
% (c) examples
% ===============================================================

%% read_lines(+C:Char) is det
%  Check the end of file 
isEOFEOL(C) :-
	C == end_of_file;
	(char_code(C,Code), Code==10).

%% read_lines(-L:List, -C:Char) is det
%  Reads the input line
read_line(L,C) :-
	get_char(C),
	(isEOFEOL(C), L = [], !; read_line(LL,_), [C|LL] = L).

%% read_lines(-Ls:List) is det
%  Reads the input lines
read_lines(Ls) :-
	read_line(L,C),
	( C == end_of_file, Ls = [] ; read_lines(LLs), Ls = [L|LLs]).

% ===============================================================
% Check and prepare the input functions
% ===============================================================

%% writeErr(+Message:String) is det
%  Write error into the stderr stream
writeErr(String) :- set_output(user_error), writeln(String).

%% chkAnyRules(+Input:List) is det
%  Check if any rule found, if not tape is tape without modifications
chkAnyRules([H|_]):-
	( [_,_,_,_,_,_,_] = H -> true
	; atomic_list_concat(H, Atom), atom_string(Atom, String), 
	  writeln(String),halt(0)).

%% checkFunctionSymbol(+Symbol:Char) is det
%  Check the target symbol to be correct
checkFunctionSymbol(Symbol) :- 
	( char_type(Symbol,lower) -> true
	; Symbol == 'R' -> true
	; Symbol == 'L' -> true
	; false).

%% checkAlphaSymbol(+Symbol:Char) is det
%  Check the source symbol to be correct
checkAlphaSymbol(Symbol) :-
	( char_type(Symbol,lower) -> true
	; Symbol == ' ' -> true
	; false).

%% checkInput(+Rules:List) is det
%  Check the input to be correct or stop the process
checkInput(Rules) :- 
	(\+checkInputRules(Rules) -> writeErr('Wrong input!'), halt(1) ; true).

%% checkInputRules(+Rules:List) is det
%  Check the rules to be correct
checkInputRules([]) :- true.
checkInputRules([H|L]) :-  
	[OldState,SpaceOne,OldAlpha,SpaceTwo,NewState,SpaceThree,NewAlpha] = H,
	char_code(SpaceOne, 32),char_code(SpaceTwo, 32),char_code(SpaceThree, 32),
	checkAlphaSymbol(OldAlpha),checkFunctionSymbol(NewAlpha),
	char_type(OldState, upper),char_type(NewState, upper),
	checkInputRules(L).

%% cleanRules(+Rules:List, -Output:List) is det
%  Remove the whitespaces from the rules
cleanRules([],Cleaned) :- Cleaned = [] .
cleanRules([H|L], Cleaned) :-
	[OldState,_,OldAlpha,_,NewState,_,NewAlpha] = H,
	cleanRules(L,CurrCleaned),
	pushFront([OldState,OldAlpha,NewState,NewAlpha], CurrCleaned, Cleaned).

% ===============================================================
% Helper functions
% ===============================================================

%% pushFront(+Item:Any, +List:List, -Output:List) is det
%  Push the element in front of the list
pushFront(Item, List, [Item|List]).

%% putStartState(+List:List, -Output:List) is det
%  Push the start symbol in front of the list
putStartState(List, ['S'|List]).

%% readPosition(+Tape:List, -State:Char, -Alpha:Char) is det
%  Get the actual position with state and symbol
readPosition([H|L],State,Alpha) :-
	( char_type(H, upper) -> State = H,
	  ( L \= [] -> [M|_] = L, Alpha = M ; Alpha = ' ') 
	; L \= [] -> readPosition(L,State,Alpha)	
	).

%% readRule(+Rules:List, +State:Char, +Alpha:Char, -Rule:List) is nondet
%  Get the actual used rule for the input alpha and state
readRule([],_,_,[]). 
readRule([H|L],State,Alpha,Rule) :-
	[OldState,OldAlpha,_,_] = H,
	( OldState==State, OldAlpha==Alpha,
	  Rule = H
	; readRule(L,State,Alpha,Rule)	
	).

%% printOutput(+StartTape:List, +Journey:List) is det
%  Print the output with start tape
printOutput(StartTape,Journey) :-
	pushFront(StartTape,Journey,JourneyWithStart), 
	printConfigutations(JourneyWithStart).

%% printConfigutations(+Journey:List) is det
%  Print a journey with all configurations
printConfigutations([]) :- true.
printConfigutations([H|L]) :- 
	atomic_list_concat(H, Atom), atom_string(Atom, String),
	writeln(String),
	printConfigutations(L).

% ===============================================================
% Turing machine functions
% ===============================================================

%% updateTape(+Tape:List, +Rule:List, -Output:List) is det
%  Update the state and symbol under head of the machine
%  Simply parse the rule and create new sequence, then push
updateTape([H|L],Rule,Output) :-
	[OldState,_,NewState,NewAlpha] = Rule,
	( H == OldState-> [_|N] = L,	
	  pushFront(NewAlpha,N,NewTail),
	  pushFront(NewState,NewTail,Output)
	; updateTape(L,Rule,CurrOutput),
	  pushFront(H,CurrOutput,Output)
	).

%% moveRightTape(+Tape:List, +Rule:List, -Output:List) is det
%  Move right in the tape and update the state of the machine
%  Simply parse the rule and create new sequence, the push
moveRightTape([H|L],Rule,Output) :-
	[OldState,_,NewState,_] = Rule,
	( H == OldState -> 
      ( L == [] -> 
	    pushFront(' ',[NewState],Output)
	  ; [M|N] = L,
	    pushFront(NewState,N,PhaseOne),
	    pushFront(M,PhaseOne,Output)
	  )  
	; moveRightTape(L,Rule,CurrOutput),
	  pushFront(H,CurrOutput,Output)
	).	

%% moveLeftTape(+Tape:List, +Rule:List, -Output:List) is det
%  Move left in the tape and update the state of the machine
%  This looks for the next character, then composite the sequence and push
moveLeftTape([H|L],Rule,Output) :-
	[OldState,_,NewState,_] = Rule,
	( H == OldState -> writeErr('Move left: Out of range!'), halt(1)
	; [M|N] = L,
	  ( M == OldState,
	    pushFront(H,N,PhaseOne),
	    pushFront(NewState,PhaseOne,Output)
	  ; moveLeftTape(L,Rule,CurrOutput),
	    pushFront(H,CurrOutput,Output)
	)).	

% ===============================================================
% The turing machine
% ===============================================================

%% doSimulationStep(+Function:Char, +Rule:List, +Tape:List, -NewTape:List) is nondet
%  Do the simulation step and update the tape
%  Choose between functions and call the requested function
doSimulationStep(Function,Rule,Tape,NewTape) :-
	( Function == 'L' -> moveLeftTape(Tape,Rule,NewTape)
	; Function == 'R' -> moveRightTape(Tape,Rule,NewTape)
	; updateTape(Tape,Rule,NewTape)).

%% simulation(+Tape:List, +Rules:List, -Journey:List) is nondet
%  Start the turing machine simulation process
%  Read alpha and state under the head, process while final state
simulation(Tape, Rules, Journey) :-
	readPosition(Tape,State,Alpha),
	( State == 'F' -> true
	; %random_permutation(Rules,NewRules),
	  reverse(Rules,NewRules),
	  readRule(NewRules,State,Alpha,Rule),
	  [_,_,_,NewAlpha] = Rule,
	  doSimulationStep(NewAlpha,Rule,Tape,NewTape),
	  simulation(NewTape,Rules,CurrJourney),
	  pushFront(NewTape,CurrJourney,Journey
	)).
	
% ===============================================================
% Entry
% ===============================================================

%% start() is nondet
%  This is the entry point called on program start
%  The program function described in the head comment
start :-
	prompt(_, ''),
	read_lines(RawInput),
	chkAnyRules(RawInput),
	last(RawInput,Tape),
	putStartState(Tape,TapeWithStart),
	delete(RawInput,Tape,Rules),
	checkInput(Rules),
	cleanRules(Rules,CleanRules),!,
	simulation(TapeWithStart,CleanRules,Journey),
	printOutput(TapeWithStart,Journey),
	halt.
