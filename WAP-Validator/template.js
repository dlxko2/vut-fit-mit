function require(elementClass) {

   var element = document.getElementById(elementClass);

   if (element.hasAttribute("valRequired"))
       element.removeAttribute("valRequired");
   else
       element.setAttribute('valRequired', "");

    checkField(element);
}

function optional(elementClass) {

    var element = document.getElementById(elementClass);

    if (element.hasAttribute("valOptional"))
        element.removeAttribute("valOptional");
    else
        element.setAttribute('valOptional', "");

    checkField(element);
}

function setTextConstraint(sourceElement, elementClass, type) {

    var attribute;
    if (type == 'minimal')  attribute = 'valMinLength';
    else if (type == 'maximal') attribute = 'valMaxLength';
    else if (type == 'regexp') attribute = 'valRegExp';

    var value = sourceElement.value;
    var empty = sourceElement.value.length;
    var element = document.getElementById(elementClass);

    if (empty == 0) {
        element.removeAttribute(attribute);
        sourceElement.classList.remove("blue");
    }
    else {
        element.setAttribute(attribute, value);
        sourceElement.classList.add("blue");
    }

    checkField(element);
}

function showHelp() {

    var destElement = document.getElementById('help');
    if (!destElement.classList.contains('dontShow'))
        destElement.classList.add('dontShow');
    else
        destElement.classList.remove('dontShow');
}

function showInfo() {

    var destElement = document.getElementById('info');
    if (!destElement.classList.contains('dontShow'))
        destElement.classList.add('dontShow');
    else
        destElement.classList.remove('dontShow');
}