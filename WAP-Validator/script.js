/**
 * Main function to check the field
 * Runs the checks for all constraints
 * @param domElement - the element of field
 */
function checkField(domElement) {

    var validator = new FormValidator(domElement);
    validator.checkRequired();
    validator.checkMaximum();
    validator.checkMinimum();
    validator.checkRegexp();
    validator.checkOptional();
    validator.changeClass();
}

var FormValidator = (function(domElement) {

    this.element = domElement;
    this.error = false;

    /**
     * Function to check if required
     * Looks up if any input length or returns the error
     */
    this.checkRequired = function () {
        if (this.element.hasAttribute('valRequired')) {
            if (this.element.value.length < 1)
                this.error = true;
        }
    };

    /**
     * Function to check if reached the maximum value
     * Looks up if input length is lower then maximum or returns the error
     */
    this.checkMaximum = function () {
        if (this.element.hasAttribute('valMaxLength')) {
            if (this.element.value.length > this.element.getAttribute("valMaxLength"))
                this.error = true;
        }
    };

    /**
     * Function to check if reached the maximum value
     * Looks up if input length is lower then maximum or returns the error
     */
    this.checkMinimum = function () {
        if (this.element.hasAttribute('valMinLength')) {
            if (this.element.value.length < this.element.getAttribute("valMinLength"))
                this.error = true;
        }
    };

    /**
     * Function to check if input matches the regexp from attribute
     * Creates the regexp and get the input - test for the match
     */
    this.checkRegexp = function () {
        if (this.element.hasAttribute('valRegExp')) {
            const regex = new RegExp(this.element.getAttribute("valRegExp"), 'g');
            const str = this.element.value;
            if (regex.exec(str) == null)
                this.error = true;
        }
    };

    /**
     * Function to check for optional parameter
     * If the length is 0 and optional don't look to other constraints
     */
    this.checkOptional = function () {
        if (this.element.hasAttribute('valOptional')) {
            if (this.element.value.length == 0) {
                this.error = false;
            }
        }
    };

    /**
     * Function to change the class using the error variable
     * Look into the error flag and choose the right color
     */
    this.changeClass = function () {

        this.element.classList.remove("red");
        this.element.classList.remove("green");

        if (this.error) {
            this.element.className = 'red';
            return;
        }

        if (this.element.hasAttribute('valOptional')) {
            if (this.element.value.length == 0)
                return;
        }

        if (this.element.type == 'submit')
            return;

        this.element.className = 'green';
    }

});

/**
 * Function processed after page load
 * Add trigger to oninput to every input element
 */
window.onload = function () {
    var fields = document.getElementsByTagName('input');
    for (var i = 0; i < fields.length; i++) {
        checkField(fields.item(i));
        fields.item(i).oninput = function () {
            checkField(this);
        }
    }
};
