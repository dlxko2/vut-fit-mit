/*H**********************************************************************
* FILENAME     : mm.c
* DESCRIPTION  : Implementation of parallel Matrix Multiplication algorithm
* AUTHOR       : Martin Pavelka
* FINAL DATE   : 24.06.2017
* EMAIL        : xpavel27@stud.fit.vutbr.cz
*H**********************************************************************/

#include <iostream>
#include <mpi.h>
#include <fstream>
#include <vector>

/* Tags for MPI */
#define T_M1_ROWS 1
#define T_M1_COLS 2
#define T_M2_ROWS 3
#define TAG_LEFT 4
#define TAG_TOP 5
#define TAG_FINAL 6

#define CONTROL 0
#define PROFILE 0
using namespace std;

/* This is matrix info struct */
struct matrixStructure {
    int columns; int rows;
    vector<int> numbers;
};

/* The main processors functions */
void funcControlProcessor(int processors, int myID);
void funcOtherProcessor(int myID, int processors);

/* Get the next positions for processors */
int getProcessorOnRight(int processorNumber) ;
int getProcessorOnTop(int processorNumber, int columns) ;
int getProcessorOnDown(int processorNumber, int columns) ;
int getProcessorOnLeft(int processorNumber, int actualRow, int columns) ;

/* Matrix basic operations */
matrixStructure parseMatrix(string inputFile, bool firstIsRows);
int getValueFromMatrix(matrixStructure matrix, int row, int col) ;
void printMatrix(matrixStructure matrix) ;

/**
 * Main function
 * @param argc - arguments count
 * @param argv - arguments data
 * @return boolean - true on finish
 */
int main(int argc, char *argv[]) {

    // Initialize the variables
    int myID;
    int numProcessors;

    // Initialize the MPI
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcessors);
    MPI_Comm_rank(MPI_COMM_WORLD, &myID);

    // Run processors routines
    if (myID == CONTROL) funcControlProcessor(numProcessors, myID);
    else funcOtherProcessor(myID, numProcessors);

    // Finalize
    MPI_Finalize();
    return EXIT_SUCCESS;
}

/**
 * Parse the input matrix file into the special structure
 * @param inputFile - the input file name
 * @param firstIsRows - if the number is rows or cols
 * @return matrixStructure - parsed matrix
 */
matrixStructure parseMatrix(string inputFile, bool firstIsRows) {

    // Open file and check
    fstream file(inputFile);
    if (!file.is_open()) perror("Can't open the input file");

    // Init structure and rows
    matrixStructure output;
    if (firstIsRows) file >> output.rows;
    else file >> output.columns;

    // Get numbers
    int number;
    while (file >> number) output.numbers.push_back(number);

    // Get columns
    if (firstIsRows) output.columns = (int) (output.numbers.size() / output.rows);
    else output.rows = (int) (output.numbers.size() / output.columns);
    return output;
}

/**
 * Functionality for the other processors implemented here
 * @param myID - the current processor ID
 * @param processors - the total number of processors
 */
void funcOtherProcessor(int myID, int processors) {

    // Initialize locals
    MPI_Status stat;
    int matrixOneColumns = 0,matrixOneRows = 0,matrixTwoColumns = 0;
    int localValue = 0,verticalValue = 0,horizontalValue = 0;

    // Get the init matrix informations
    MPI_Recv(&matrixOneColumns, 1, MPI_INT, 0, T_M1_COLS, MPI_COMM_WORLD, &stat);
    MPI_Recv(&matrixOneRows, 1, MPI_INT, 0, T_M1_ROWS, MPI_COMM_WORLD, &stat);
    MPI_Recv(&matrixTwoColumns, 1, MPI_INT, 0, T_M2_ROWS, MPI_COMM_WORLD, &stat);
    int rowId = ( myID / matrixTwoColumns ) + 1;

    // While the all data not parsed
    for (int i = 0; i < matrixOneColumns; i++) {

        // Init neighbour processors
        int leftProcessor = getProcessorOnLeft(myID,rowId,matrixTwoColumns);
        int topProcessor = getProcessorOnTop(myID,matrixTwoColumns);

        // Receive the data from the neighbours and calculate
        MPI_Recv(&horizontalValue, 1, MPI_INT, leftProcessor, TAG_LEFT, MPI_COMM_WORLD, &stat);
        MPI_Recv(&verticalValue, 1, MPI_INT, topProcessor, TAG_TOP, MPI_COMM_WORLD, &stat);
        localValue += horizontalValue * verticalValue;

        // Get horizontal
        if (myID+1 != processors)
            MPI_Send(&horizontalValue, 1, MPI_INT, getProcessorOnRight(myID), TAG_LEFT, MPI_COMM_WORLD);

        // Get vertical
        if (myID+matrixTwoColumns < processors)
            MPI_Send(&verticalValue, 1, MPI_INT, getProcessorOnDown(myID, matrixTwoColumns), TAG_TOP, MPI_COMM_WORLD);
    }

    // Send final value to control processor
    MPI_Send(&localValue, 1, MPI_INT, 0, TAG_FINAL, MPI_COMM_WORLD);
}

/**
 * Functionality for the controll processor implemented here
 * @param processors - the total number of processors
 * @param myID - the current processor id
 */
void funcControlProcessor(int processors, int myID) {

    // Init locals and file matrix one and two
    MPI_Status stat;
    int localValue = 0, verticalValue = 0, horizontalValue = 0;
    matrixStructure matrixOne = parseMatrix("mat1", true);
    matrixStructure matrixTwo = parseMatrix("mat2", false);

    // Send initialization of matrix info to other processors
    for (int i = 1; i < processors; i++) {
        MPI_Send(&matrixOne.columns, 1, MPI_INT, i, T_M1_COLS, MPI_COMM_WORLD);
        MPI_Send(&matrixOne.rows, 1, MPI_INT, i, T_M1_ROWS, MPI_COMM_WORLD);
        MPI_Send(&matrixTwo.columns, 1, MPI_INT, i, T_M2_ROWS, MPI_COMM_WORLD);
    }

    // While any data to be parsed in the inputs
    double startTime = MPI_Wtime();
    for (int i = 0; i < matrixOne.columns; i++) {

        // Get values from the input and calculate
        horizontalValue = getValueFromMatrix(matrixOne, 0+1, matrixOne.columns-i);
        verticalValue = getValueFromMatrix(matrixTwo, matrixOne.columns-i, 0+1);
        localValue += horizontalValue * verticalValue;

        // Send horizontals
        if (myID + 1 != processors && matrixTwo.columns != 1)
            MPI_Send(&horizontalValue, 1, MPI_INT, getProcessorOnRight(myID), TAG_LEFT, MPI_COMM_WORLD);

        // Send verticals
        if (myID + matrixTwo.columns < processors)
            MPI_Send(&verticalValue, 1, MPI_INT, getProcessorOnDown(myID, matrixTwo.columns), TAG_TOP, MPI_COMM_WORLD);

        // Send another horizontals
        for (int j = 1; j < matrixOne.rows; j++) {
            int value = getValueFromMatrix(matrixOne,j+1,matrixOne.columns-i);
            MPI_Send(&value, 1, MPI_INT, matrixTwo.columns * j, TAG_LEFT, MPI_COMM_WORLD);
        }

        // Send another verticals
        for (int j = 1; j < matrixTwo.columns; j++) {
            int value = getValueFromMatrix(matrixTwo,matrixTwo.rows-i,j+1);
            MPI_Send(&value, 1, MPI_INT, j, TAG_TOP, MPI_COMM_WORLD);
        }
    }

    // Create the output matrix and push control value
    matrixStructure outputMatrix = matrixStructure();
    outputMatrix.numbers.push_back(localValue);

    // Get results from other processors and fill up the matrix
    for (int i = 1; i < processors; i++) {
        int receivedData = 0;
        MPI_Recv(&receivedData, 1, MPI_INT, i, TAG_FINAL, MPI_COMM_WORLD, &stat);
        outputMatrix.numbers.push_back(receivedData);
    }

    // If profiling print the profile output
    double endTime = MPI_Wtime();
    if (PROFILE) cout << matrixOne.columns << ';' << 4 << ";" << (int)((endTime-startTime)*1000000) << endl;

    // If not profile print the output
    if (!PROFILE) {
        outputMatrix.rows = matrixOne.rows;
        outputMatrix.columns = matrixTwo.columns;
        printMatrix(outputMatrix);
    }
}

/**
 * Print the output matrix on the stdout
 * @param matrix - the input matrix structure
 */
void printMatrix(matrixStructure matrix) {

    // Print the first line
    cout << matrix.rows << ":" << matrix.columns;

    // Print the values
    for (int i = 0; i < matrix.numbers.size(); i++) {
        if(i % matrix.columns == 0) cout << endl;
        cout << matrix.numbers[i];
        if (i% matrix.columns != matrix.columns -1) cout << " ";
    }

    // Print the newline at the end?
    cout << endl;
}

/**
 * Function to get the value from the matrix numbers vector
 * @param matrix - the input matrix structure
 * @param row - the row requested
 * @param col - the col requested
 * @return - the number on the position
 */
int getValueFromMatrix(matrixStructure matrix, int row, int col) {
    return matrix.numbers[matrix.columns * (row-1) + (col - 1)];
}

/**
 * Get the neighbour processor on the right
 * @param processorNumber - the actual processor number
 * @return - the right processor
 */
int getProcessorOnRight(int processorNumber) {
    return processorNumber+1;
}

/**
 * Get the processor on the left
 * @param processorNumber - the actual processor number
 * @param actualRow - the actual row
 * @param columns - the columns count
 * @return
 */
int getProcessorOnLeft(int processorNumber, int actualRow, int columns) {
    int output = processorNumber-1;
    int outputRow = (output / columns) +1;
    if (actualRow != outputRow) return 0;
    return output;
}

/**
 * Get the processor on the top
 * @param processorNumber - the actual processor
 * @param columns - the columns number
 * @return - the processor id on the top
 */
int getProcessorOnTop(int processorNumber, int columns) {
    int output = processorNumber-columns;
    if (output < 0) return 0;
    else return output;
}

/**
 * Get the bottom processor
 * @param processorNumber - the actual processor
 * @param columns - the columns number
 * @return - the processor on the bottom
 */
int getProcessorOnDown(int processorNumber, int columns) {
    return  processorNumber+columns;
}