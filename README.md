# README #

This repository includes all my school project in 
Information Systems
Faculty of Informatics
Brno University of Technology 

### What is this repository for? ###

This repository is for my and another people who
wants to find out some help in their projects on 
this school.

### How do I get set up? ###

Each branch includes the project for the subject.
The master repo includes the everything.
There are pipelines for each branch.

### Who do I talk to? ###

Martin Pavelka
xpavel27@stud.fit.vutbr.cz
about.me/dlxko