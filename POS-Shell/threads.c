/*h*********************************************************************
 The threads source file
 Implements the threads functions
 Revision: 28.04.2017
 ***********************************************************************/

#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include "helper.h"
#include "parser.h"
#include "signals.h"
#include "threads.h"

pthread_cond_t condition = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
bool workingProgress;
bool finishingProgress;
pid_t childPid;

/**
 * This is the thread for parsing the command
 * @param pVoid - the unused but requested argument
 * @return void - just pthread_exit function call
 */
void *workerThreadRoutine(void *pVoid) {

    /* This is just hack for unused parameter */
    (void) (pVoid);

    /* While flag after exit command used */
    while (!finishingProgress) {

        /* Init variables like C90 */
        char *input, *output, *command, **arguments;
        bool exit;
        int ampersand;

        /* Wait until something is read in first thread */
        pthread_mutex_lock(&mutex);
        while (workingProgress == false) pthread_cond_wait(&condition, &mutex);

        /* Parse the input set by the first thread */
        input = parseInput();
        output = parseOutput();
        command = parseCommand();
        exit = parseExitCommand();
        ampersand = parseAmpersand();
        arguments = parseArguments(command);

        /* This is the debug */
        if(DEBUG) {
            printf("Input:%s\n",input);
            printf("Output:%s\n",output);
            printf("Command:%s\n",command);
            printf("Ampersand:%d\n",ampersand);
            printf("Arg1:%s\n",arguments[0]);
            printf("Arg2:%s\n",arguments[1]);
        }

        /* If exit command skip the another process */
        if (exit) finishingProgress = true;

        /* Only when we have any command we process */
        if (!exit && arguments != NULL && arguments[0] != NULL) {

            /* The child process */
            if ((childPid = fork()) == 0) {

                /* Redirect the stdout and stdin */
                int out = redirectOutput(output);
                int in = redirectInput(input);
                if (in == WRONG_DESCRIPTOR || out == WRONG_DESCRIPTOR) _exit(EXIT_FAILURE);

                /*  If we have backgroud process */
                if (ampersand == AMPERSAND_FOUND) { setsid(); ignoreSignals();}

                /* Run the command and terminate the calling process */
                if (execvp(arguments[0], arguments) == -1) {
                    perror("Error while running the command.");
                    if (out != EMPTY_DESCRIPTOR) close(out);
                    if (in != EMPTY_DESCRIPTOR) close(in);
                    _exit(EXIT_FAILURE);
                }

                /* Do the normal exit */
                close(out); close(in);
                _exit(EXIT_SUCCESS);
            }

            /* The parent process */
            else if (childPid > 0) {

                /* Init child wait or kill signals */
                if (ampersand == AMPERSAND_FOUND) initChildWait();
                else { initKillSignal(); wait(NULL); }
            }

            /* Report the error */
            else if (childPid < 0) perror("Error while forking.");
        }

        /* Do the cleaning up */
        if (output != NULL) free(output);
        if (input != NULL) free(input);
        if (command != NULL) free(command);

        /* Inform another thread */
        workingProgress = false;
        pthread_cond_signal(&condition);
        pthread_mutex_unlock(&mutex);
    }

    pthread_exit(NULL);
}

/**
 * This is the thread for reading the input
 * @param pVoid - this is the unused
 * @return void - just pthread_exit function call
 */
void *readerThreadRoutine(void *pVoid) {

    /* Just the unused workaround */
    (void) (pVoid);
    workingProgress = false;
    finishingProgress = false;

    /* While the second thread report exit */
    while (!finishingProgress) {

        /* Init variables like C90 */
        bool wrongSize;

        /* Initialize the read process */
        pthread_mutex_lock(&mutex);
        wrongSize = false;
        printf("$ "); fflush(stdout);
        signal(SIGINT,intHandler);

        while (true) {

            /* Init variables like C90 */
            ssize_t readSize;

            /* Try to read from input or */
            memset(iBuff, 0, INPUT_BUFFER_SIZE);
            readSize = read(STDIN_FILENO, &iBuff, INPUT_BUFFER_SIZE);
            if (readSize > INPUT_BUFFER_SIZE - 1 || readSize == -1) wrongSize = true;

            /* When end of line we break, if cmd-d we stop */
            if (iBuff[readSize - 1] == '\n') break;
            if (readSize == 0) { strcpy(iBuff, "exit"); break; }
        }

        /* Wrong size continue */
        if (wrongSize) {
            fprintf(stderr, "Wrong input data, please try again!\n");
            pthread_mutex_unlock(&mutex);
            continue;
        }

        /* Signal the another thread */
        workingProgress = true;
        pthread_cond_signal(&condition);

        /* Wait until shell thread finish */
        while (workingProgress) pthread_cond_wait(&condition, &mutex);
        pthread_mutex_unlock(&mutex);
    }

    pthread_exit(NULL);
}

/**
 * Redirect the stdout to the file
 * @param output - the output file
 * @return int - the file descriptor (-1error,-2nothing,+descriptor)
 * @see http://codewiki.wikidot.com/c:system-calls:dup
 */
int redirectOutput(char *output) {

    /* Initialize like C90 */
    int fd = 0;

    /* If nothing do nothing */
    if (output == NULL) return EMPTY_DESCRIPTOR;

    /* Open the file or return the error */
    fd = open(output, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    if (fd < 0) { perror("Can't open the output file."); return WRONG_DESCRIPTOR; }

    /* Close the stdout */
    if (close(STDOUT_FILENO) < 0) {
        close(fd);
        perror("Can't close the stdout.");
        return WRONG_DESCRIPTOR;
    }

    /* Duplicate the descriptor */
    if (dup(fd) != STDOUT_FILENO) {
        close(fd);
        perror("Can't duplicate descriptor.");
        return WRONG_DESCRIPTOR;
    }
    return fd;
}

/**
 * Redirect the stdin to the file
 * @param output - the input file
 * @return int - the file descriptor (-1error,-2nothing,+descriptor)
 * @see http://codewiki.wikidot.com/c:system-calls:dup
 */
int redirectInput(char *input) {

    /* Initialize like C90 */
    int fd = 0;

    /* If nothing do nothing */
    if (input == NULL) return EMPTY_DESCRIPTOR;

    /* Open the file or return the error */
    fd = open(input, O_RDONLY);
    if (fd < 0) { perror("Can't open the input file"); return WRONG_DESCRIPTOR; }

    /* Close the stdout */
    if (close(STDIN_FILENO) < 0) {
        close(fd);
        perror("Can't close the stdin");
        return WRONG_DESCRIPTOR;
    }

    /* Duplicate the descriptor */
    if (dup(fd) != STDIN_FILENO) {
        close(fd);
        perror("Can't dup");
        return WRONG_DESCRIPTOR;
    }
    return fd;
}
