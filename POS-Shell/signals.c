/*h*********************************************************************
 The signals source file
 Implements the signals functions
 Revision: 28.04.2017
 ***********************************************************************/

#define _GNU_SOURCE

#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include "helper.h"
#include "signals.h"

/** The function to ignore the signals **/
void ignoreSignals() {
    signal(SIGINT,SIG_IGN);  /* Keyboard exit */
    signal(SIGQUIT,SIG_IGN); /* Keyboard quit */
    signal(SIGTTIN,SIG_IGN); /* Termination signal */
    signal(SIGTSTP,SIG_IGN); /* Interactive stop signal */
}

/** The signal init for waiting for child
 *  @see http://pubs.opengroup.org/onlinepubs/009695399/functions/sigaction.html
 */
void initChildWait() {

    /* Initialize the signal structure */
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = childWaitHandler;

    /* Set the signal handler */
    if (sigaction(SIGCHLD, &sa, NULL) == -1)
        perror("Problem while initializing SIGCHLD");
}

/** The signal init killing the process **/
void initKillSignal() {

    /* Initialize the structure */
    struct sigaction sa;
    sa.sa_handler = killHandler;
    sigemptyset(&sa.sa_mask);

    /* Set the signal handler */
    if (sigaction(SIGINT, &sa, NULL) == -1)
        perror("Problem while initializing SIGCHLD");
}

/**
 * Perform waiting for the child exist
 * @param sig - the signal handled
 * @see https://linux.die.net/man/2/waitpid
 */
void childWaitHandler(int sig) {
    pid_t childPidLocal;
    while ((childPidLocal = waitpid(-1, NULL, WNOHANG)) > 0) {
        if (SIGCHLD == sig) {
            printf("Process (%d) finished.\n", childPidLocal);
            printf("$ ");
            fflush(stdout);
        }
    }
}

/**
 * Perform waiting for the child exist
 * @param sig - the signal handled
 */
void killHandler(int sig) {
    if (SIGINT == sig) {
        printf("\n");
        fflush(stdout);
    }
}

/**
 * Disable killing main
 * @param sig - signal number
 */
void intHandler(int sig) {
    (void) (sig);
    printf("\n$ ");
    fflush(stdout);
}

