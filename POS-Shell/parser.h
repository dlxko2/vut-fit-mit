/*h*********************************************************************
 The parser header file
 Implements the parser functions
 Revision: 28.04.2017
 ***********************************************************************/

char *parseCommand();
char *parseOutput();
char *parseInput();
int parseAmpersand();
bool parseExitCommand();
bool isWhiteSpace(char input);
char **parseArguments(char *command);
