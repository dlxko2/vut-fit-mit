/*h*********************************************************************
 The threads source file
 Implements the threads functions
 Revision: 28.04.2017
 ***********************************************************************/

void intHandler(int sig);
void ignoreSignals();
void initChildWait();
void initKillSignal();
void killHandler(int sig);
void childWaitHandler(int sig) ;
