/*h*********************************************************************
 The helper header file
 Implements the helper defines
 Revision: 28.04.2017
 ***********************************************************************/

#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <memory.h>
#include <errno.h>

#define INPUT_BUFFER_SIZE 513
#define DEBUG 0
#define WRONG_DESCRIPTOR -1
#define EMPTY_DESCRIPTOR -2
#define CHAR_TAB '\t'
#define CHAR_NEWLINE '\n'
#define CHAR_AMPERSAND '&'
#define CHAR_IN_PIPE '<'
#define CHAR_OUT_PIPE '>'
#define AMPERSAND_WRONG 2
#define AMPERSAND_FOUND 1
#define AMPERSAND_NONE 0

char iBuff[INPUT_BUFFER_SIZE];

