/*h*********************************************************************
 The parser source file
 Implements the parser functions
 Revision: 28.04.2017
 ***********************************************************************/

#include "helper.h"
#include "parser.h"

/**
 * Parse the command from the buffer
 * @return char* the parsed command
 */
char *parseCommand() {

    /* Initialize like C90 */
    char *output = NULL;
    int oP = 0;
    int iBP = 0;

    /* List the buffer */
    for (iBP = 0; iBP <= strlen(iBuff); iBP++) {

        /* Saving the output */
        if (iBuff[iBP] != CHAR_AMPERSAND && iBuff[iBP] != CHAR_IN_PIPE &&
            iBuff[iBP] != CHAR_OUT_PIPE && iBuff[iBP] != CHAR_NEWLINE) {
            if (oP == 0) output = calloc(1, sizeof(char));
            else output = realloc(output, iBP * sizeof(char) + 1);
            output[oP] = iBuff[iBP];
            ++oP;
        }

        /* Start parsing the output */
        if (iBuff[iBP] == CHAR_OUT_PIPE || iBuff[iBP] == CHAR_IN_PIPE || iBuff[iBP] == CHAR_AMPERSAND) break;

    } return output;
}

/**
 * Parse the output from the buffer
 * @return - char* the parsed output ( > output )
 */
char *parseOutput() {

    /* Init the locals */
    char *output = NULL;
    bool fillingTheBuffer = false;
    int oBP = 0;
    int iBP = 0;

    /* List the buffer */
    for (iBP = 0; iBP <= strlen(iBuff); iBP++) {

        /* Start parsing the output */
        if (iBuff[iBP] == CHAR_OUT_PIPE) { fillingTheBuffer = true; continue; }
        if (iBuff[iBP] == CHAR_IN_PIPE && fillingTheBuffer) break;

        /* Saving the output */
        if (fillingTheBuffer && !isWhiteSpace(iBuff[iBP]) && iBuff[iBP] != CHAR_AMPERSAND) {
            if (oBP == 0)  output = calloc(1, sizeof(char));
            else output = realloc(output, oBP * sizeof(char) + 1);
            output[oBP] = iBuff[iBP];
            ++oBP;
        }
    }

    return output;
}

/**
 * Parse the input from the buffer
 * @return - char* the parsed input ( < input )
 */
char *parseInput() {

    /* Init like C90 */
    char *output = NULL;
    bool fillingTheBuffer = false;
    int oBP = 0;
    int iBP = 0;

    /* List the buffer */
    for (iBP = 0; iBP <= strlen(iBuff); iBP++) {

        /* Start parsing the input */
        if (iBuff[iBP] == CHAR_IN_PIPE) { fillingTheBuffer = true; continue; }

        /* Saving the input */
        if (fillingTheBuffer && !isWhiteSpace(iBuff[iBP]) && iBuff[iBP] != CHAR_AMPERSAND) {
            if (iBuff[iBP] == CHAR_OUT_PIPE) break;
            if (oBP == 0) output = calloc(1, sizeof(char));
            else output = realloc(output, oBP * sizeof(char) + 1);
            output[oBP] = iBuff[iBP];
            ++oBP;
        }
    } return output;
}

/**
 * Function to parse the ampersand
 * @return - (0notFound, 1justFound, 2wrongPosition)
 */
int parseAmpersand() {

    /* Initialize like C90 */
    int iBP = 0;

    /* Browse the buffer */
    bool characterFound = false;
    for (iBP = 0; iBP <= strlen(iBuff); iBP++) {

        /* Found the ampersand and check the wrong position */
        if (iBuff[iBP] == CHAR_AMPERSAND) { characterFound = true; continue; }
        if (characterFound && !isWhiteSpace(iBuff[iBP])) return AMPERSAND_WRONG;
    }

    /* Convert the result */
    return (characterFound) ? AMPERSAND_FOUND : AMPERSAND_NONE;
}

/**
 * Create the arguments array for the exec
 * @param command - the input parsed command
 * @return **char - the arguments array
 */
char **parseArguments(char *command) {

    /* Init like C90 */
    int argC = 0;
    char **output = NULL;
    int iCP = 0;

    /* If nothing return nothing */
    if (command == NULL) return NULL;

    /* Alloc locals */
    output = calloc(1, sizeof(char*));

    /* List the command buffer */
    for (iCP = 0; iCP <= strlen(command); iCP++) {

        /* Init the locals for the iteration */
        char *argument = calloc(1, sizeof(char));
        int argP = 0;
        int iAP;

        /* List the argument */
        for (iAP = iCP; iAP <= strlen(command); iAP++) {

            if (!isWhiteSpace(command[iAP])) {
                argument = (char*) realloc(argument, argP * sizeof(char) + 1);
                argument[argP] = command[iAP];
                ++ argP;
            }

            else {
                iCP = iAP;
                if (argP > 0) {
                    output = realloc(output, argC * sizeof(char *) + 1);
                    output[argC] = argument;
                    ++ argC;
                }
                break;
            }
        }
    }

    /* Save the argument */
    output = realloc(output, argC * sizeof(char *) + 1);
    output[argC] = NULL;
    return output;
}

/** Look for the exit command **/
bool parseExitCommand() {
    if (iBuff[0] == 'e' && iBuff[1] == 'x' && iBuff[2] == 'i' && iBuff[3] == 't') return true;
    else return false;
}

/** Check the whitespace **/
bool isWhiteSpace(char input) {
    if (input == CHAR_TAB || input == CHAR_NEWLINE || input == ' ' || input == '\0') return true;
    else return false;
}
