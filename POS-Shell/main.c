/*h*********************************************************************
 Advanced Operating Systems - shell
 Martin Pavelka (xpavel27)
 xpavel27@stud.fit.vutbr.cz
 ***********************************************************************/

#include <fcntl.h>
#include <signal.h>
#include "helper.h"
#include "threads.h"
#include "signals.h"

/**
 * The main application entry point
 * @return int - the program exit code
 */
int main() {

    /* Initialize the threads */
    pthread_t readerThread;
    pthread_t workerThread;
    pthread_attr_t attrib;

    /* Set the threads arguments */
    if (pthread_attr_init(&attrib) != 0) perror("Can't init thread attribute");
    if (pthread_attr_setdetachstate(&attrib,PTHREAD_CREATE_JOINABLE) != 0) perror("Can't set the thread attributes");

    /* Create the threads */
    if (pthread_create(&readerThread, &attrib, readerThreadRoutine, NULL) != 0) perror("Can't create reader thread");
    if (pthread_create(&workerThread, &attrib, workerThreadRoutine, NULL) != 0) perror("Can't create reader thread");

    /* Destroy attributes and join the threads */
    if (pthread_attr_destroy(&attrib) != 0) perror("Can't destroy thread attributes");
    if (pthread_join(readerThread, NULL) != 0) perror("Can't join the reader thread");
    if (pthread_join(workerThread, NULL) != 0) perror("Can't join the reader thread");
    return EXIT_SUCCESS;
}


