/*h*********************************************************************
 The threads header file
 Implements the threads functions
 Revision: 28.04.2017
 ***********************************************************************/

void *readerThreadRoutine(void *pVoid);
void *workerThreadRoutine(void *pVoid);
int redirectInput(char *input);
int redirectOutput(char *output);
