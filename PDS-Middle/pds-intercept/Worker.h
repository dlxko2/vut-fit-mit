/**
 * The header file for the worker
 * @class Worker
 * @author Martin Pavelka
 * @date 14.04.2017
 */

#ifndef PDS_SCANNER_CAPTURE_H
#define PDS_SCANNER_CAPTURE_H

#include <stropts.h>
#include <sys/ioctl.h>
#include <pcap.h>
#include <netdb.h>
#include <netinet/ip.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <net/if.h>
#include <regex>

/** Definition of the class **/
class Worker {
public:
    Worker();
    pthread_t *threads;
    int threadsNumber;
    void initializeWorker(Args arguments, list <macAddressesStruc> parsedList);
private:
    // The stuff to get this work nice
    string interface;
    list<macAddressesStruc> parsedList;
    void createCapturers();
    uint8_t *srcMac;
    // Ouu some statics for the threads
    static void *capturePackets(void *arg);
    static void packetHandler(u_char *args, const pcap_pkthdr *header, const u_char *packet);
    static void convertMAC(string macAddress, uint8_t *output);

    void getSourceMAC();


};

#endif
