/**
 * This class implements the parser functionality
 * @class Parser
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#include "Helper.h"
#include "Parser.h"

/**
 * This is the constructor
 * @param inputFile - the input file to parsed
 */
Parser::Parser(string inputFile) {

    // Get the Root element
    xmlInitParser();
    xmlDoc *document = xmlParseFile (inputFile.c_str());
    xmlNode *rootElement = NULL;
    rootElement = xmlDocGetRootElement(document);

    // Parse
    this->parseInput(rootElement);

    // Cleanup
    xmlFreeDoc(document);
    xmlCleanupParser();
}

/**
 * Function to parse the root and childs
 * @param rootElement - the root element
 */
void Parser::parseInput(xmlNode * rootElement){

    // Initialize elements
    xmlNode *macElement = NULL;

    // For all MAC addresses
    for (macElement = rootElement->children; macElement; macElement = macElement->next) {
        if (macElement->type == XML_ELEMENT_NODE) {

            // Only for groups
            if (macElement->properties->next != NULL) {

                // Create the MAC address node
                string macAddress((char *) macElement->properties->children->content);
                string groupName((char *) macElement->properties->next->children->content);
                int groupId = atoi(groupName.substr(12).c_str());
                cout << groupId << endl;
                macAddressesStruc nodeMAC;
                nodeMAC.address = macAddress;
                nodeMAC.groupID = groupId;

                // Save the MAC
                this->parserOutput.push_front(nodeMAC);
            }
        }
    }
}