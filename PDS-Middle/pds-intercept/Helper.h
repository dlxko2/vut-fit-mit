/**
 * This class implements helpers functions to cover the minor functions
 * They are used in the whole project and covers network operations
 * @class Helpers
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#ifndef PDS_SCANNER_HELPERS_H
#define PDS_SCANNER_HELPERS_H

#include <string>
#include <iostream>
#include <cstdint>
#include <list>
#include <cstring>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fstream>
#include <linux/if_packet.h>

#define DEBUG 1
#define MAXIMUM_PACKET_SIZE 65535
#define ARP_HW_ADDRESS_LENGHT 6
#define MAC_ADDRESS_SIZE      6

using namespace std;

/** Structure of list for XML output **/
struct macAddressesStruc{
    int groupID;
    string address;
};

/** Structure for passing arguments for capture **/
struct captureStruc{
    string victimOneMAC;
    string  victimTwoMAC;
    uint8_t  *myMAC;
    string interface;
};

/** Structure for passing arguments for packet handler **/
struct packetStruc{
    uint8_t * victimOneMAC;
    uint8_t * victimTwoMAC;
    uint8_t  *myMAC;
    sockaddr_ll device;
    int socket;
};


#endif
