/**
 * This class implements the main functionality
 * @class Main
 * @author Martin Pavelka
 * @date 14.04.2017
 */

#include "pcap.h"
#include "Helper.h"
#include "Args.h"
#include "Parser.h"
#include "Worker.h"

Worker mainWorker;

/**
 * Function to handle the signals
 * @param signo - the signal number
 */
void sig_handler(int signo) {

    if (signo == SIGINT || signo == SIGUSR1) {
        printf("Received signal\n");
        exit(EXIT_SUCCESS);
    }
}

/** Function to initialize the signal handlers **/
void initializeSignals() {

    if (signal(SIGINT,  sig_handler) == SIG_ERR) cerr << "[SIGS]" << "1Can't initialize the signals" << endl;
    if (signal(SIGUSR1, sig_handler) == SIG_ERR) cerr << "[SIGS]" << "2Can't initialize the signals" << endl;
}

/**
 * The main function of program
 * @param argc - the number of arguments
 * @param argv - the array of arguments
 * @return - the integer value
 */
int main(int argc, char *argv[]) {

    // Initialize signals
    initializeSignals();

    // Get arguments - wrong argument is not exception
    if (DEBUG) cout << "Parsing the arguments" << endl;
    Args mainArgs(argc, argv);
    if (mainArgs.error.length() > 0) {
        cerr << "Args:" << mainArgs.error << endl;
        return EXIT_FAILURE;
    }

    // Parse input file
    if (DEBUG) cout << "Parsing the XML" << endl;
    Parser mainParser(mainArgs.inputFile);
    if (mainParser.error.length() > 0) {
        cerr << "Parser:" << mainParser.error << endl;
        return EXIT_FAILURE;
    }

    // Run the worker
    if (DEBUG) cout << "Waiting for tasks" << endl;
    mainWorker.initializeWorker(mainArgs, mainParser.parserOutput);
    for (int i = 0; i < mainWorker.threadsNumber; i++) {
        pthread_join(mainWorker.threads[i], NULL);
    }
}