/**
 * This class implements the arguments functionality
 * @class Args
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#include "Helper.h"
#include "Args.h"

/**
 * Constructor function
 * @brief initialize the arguments variables and set new ones
 * @param argc - the number of arguments
 * @param argv - the array of arguments
 */
Args::Args(int argc, char *argv[]) {

    // Initialize the structure and option variable
    int option = 0;

    // Check if we have the correct number of arguments
    if (argc != 5) {
        this->help();
        this->error = "Args::Args - wrong number of arguments";
        return;
    }

    // Use getopt to get the -i and -f arguments
    while ((option = getopt(argc, argv,"f:o:")) != -1) {
        switch (option) {
            case 'f' : this->inputFile  = optarg; break;
            case 'o' : this->outputFile  = optarg; break;
            case ':': this->help(); this->error = "Args::Args - option requires argument";return;
            case '?': this->help(); this->error = "Args::Args - wrong argument used";return;
            default: this->help();  this->error = "Args::Args - wrong argument used";return;
        }
    }
}

/**
 * Function to print the help page
 * @brief on problem with arguments write this stuff
 */
void Args::help() {

    cout << "#Author: Martin Pavelka" << endl;
    cout << "#Modified: 13.04.2017" << endl;
    cout << "#Param1: f - input file" << endl;
    cout << "#Param2: o - output file" << endl;
    cout << "#Run: ./pds-chooser -f [file] -o [file]" << endl;
}
