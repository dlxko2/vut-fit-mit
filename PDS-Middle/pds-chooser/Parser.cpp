/**
 * This class implements the parser functionality
 * @class Parser
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#include "Helper.h"
#include "Parser.h"

/**
 * This is the constructor
 * @param inputFile - the input file to parsed
 */
Parser::Parser(string inputFile) {

    // Initialize index
    this->actualIndex = 0;

    // Get the Root element
    xmlInitParser();
    xmlDoc *document = xmlParseFile (inputFile.c_str());
    xmlNode *rootElement = NULL;
    rootElement = xmlDocGetRootElement(document);

    // Parse
    this->parseInput(rootElement);

    // Cleanup
    xmlFreeDoc(document);
    xmlCleanupParser();
}

/**
 * Function to parse the root and childs
 * @param rootElement - the root element
 */
void Parser::parseInput(xmlNode * rootElement){

    // Initialize elements
    xmlNode *macElement = NULL;
    xmlNode *ipElement = NULL;

    // For all MAC addresses
    for (macElement = rootElement->children; macElement; macElement = macElement->next) {
        if (macElement->type == XML_ELEMENT_NODE) {

            // Create the MAC address node
            string macAddress((char*) macElement->properties->children->content);
            macAddressesStruc nodeMAC;
            nodeMAC.address = macAddress;
            nodeMAC.index = actualIndex;
            nodeMAC.groupID = -1;
            ++ this->actualIndex;

            // Browse all IP nodes
            for (ipElement = macElement->children; ipElement; ipElement = ipElement->next) {
                if (xmlStrcmp(ipElement->name,(const xmlChar *)"text")) {

                    // Get the IP info
                    string ipAddress((char*) xmlNodeGetContent(ipElement));
                    string ipType((char*) ipElement->name);
                    bool isIPv6 = ipType.compare("ipv6") == 0;

                    // Save the IP info
                    ipAddressesStruc nodeIP;
                    nodeIP.ipv6Predicate = isIPv6;
                    nodeIP.address = ipAddress;
                    nodeMAC.ipAddresses.push_front(nodeIP);
                }
            }

            // Save the MAC
            this->parserOutput.push_front(nodeMAC);
        }
    }
}