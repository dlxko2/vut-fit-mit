/**
 * The header file for the Asker class
 * @class Asker
 * @author Martin Pavelka
 * @date 16.04.2017
 */

#ifndef PDS_CHOOSER_ASKER_H
#define PDS_CHOOSER_ASKER_H

/** Class definition **/
class Asker {
public:
    Asker (list<macAddressesStruc> parserOutput);
    list<macAddressesStruc> askerOutput;
private:
    void WriteChoices();
    void AskChoices();
    void WriteSelected();
};


#endif
