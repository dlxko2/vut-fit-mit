/**
 * This class implements the asker functionality
 * @class Args
 * @author Martin Pavelka
 * @date 16.04.2017
 */

#include "Helper.h"
#include "Asker.h"

/**
 * This is the constructor function
 * @brief - ask the user to select the victims
 * @param parserOutput - the parsed XML structure
 */
Asker::Asker (list<macAddressesStruc> parserOutput) {

    this->askerOutput = parserOutput;
    this->WriteChoices();
    this->AskChoices();
    this->WriteSelected();
}

/** Function to write choices **/
void Asker::WriteChoices() {

    // We need to list all hosts
    for(list<macAddressesStruc>::iterator mac = askerOutput.begin(); mac != askerOutput.end(); mac++){

        // Check if any IP is available
        bool anyIP = false;
        for(list<ipAddressesStruc>::iterator ip = mac->ipAddresses.begin(); ip != mac->ipAddresses.end(); ip++)
            anyIP = true;

        // If we don't have any IPs just continue
        if (!anyIP) continue;

        // Write the host
        cout << "Choice: " << mac->index << " Host: " <<  mac->address.c_str() <<  "\n";

        // List all IP addresses
        for(list<ipAddressesStruc>::iterator iip = mac->ipAddresses.begin(); iip != mac->ipAddresses.end(); iip++) {
            if (!iip->ipv6Predicate) cout << "\tIPv4: " << iip->address.c_str() <<  "\n";
            else cout << "\tIPv6: " << iip->address.c_str() <<  "\n";
        }
    }
}

/** Function to ask about the choice **/
void Asker::AskChoices() {

    // Prepare the choice variable
    char choiceVictim1 = 0;
    char choiceVictim2 = 0;
    int actualIndex = 1;

    // Do while the character is not 'x'
    while (choiceVictim2 != 'x') {

        // Ask for the choice
        cout << "Select the index for victim 1 or type x" << endl;
        cin >> choiceVictim1;
        if (choiceVictim1 == 'x') break;

        // Ask for the second choice
        cout << "Select the index for victim 2 or type x" << endl;
        cin >> choiceVictim2;
        if (choiceVictim2 == 'x') break;

        // Convert to integer
        int selectedIndex1 = choiceVictim1 - '0';
        int selectedIndex2 = choiceVictim2 - '0';

        // List all mac addresses for the index selected - then mark it
        int foundIndexes = 0;
        for(list<macAddressesStruc>::iterator iterMAC = askerOutput.begin(); iterMAC != askerOutput.end(); iterMAC++)
            if (iterMAC->index == selectedIndex1 || iterMAC->index == selectedIndex2) ++foundIndexes;

        // Report problem with indexes
        if (foundIndexes != 2) {
            cerr << "Wrong indexes entered " << selectedIndex1 << "/" << selectedIndex2 << endl;
            continue;
        }

        // Save if correct
        for(list<macAddressesStruc>::iterator iterMAC = askerOutput.begin(); iterMAC != askerOutput.end(); iterMAC++) {
            if (iterMAC->index == selectedIndex1 || iterMAC->index == selectedIndex2) {
                iterMAC->groupID = actualIndex;
            }
        }

        // Increment
        ++actualIndex;
    }
}

/** Write the selected choices **/
void Asker::WriteSelected() {

    // List all MAC addresses
    cout << "You have selected:" << endl;
    for(list<macAddressesStruc>::iterator iterMAC = askerOutput.begin(); iterMAC != askerOutput.end(); iterMAC++){
        if (iterMAC->groupID == -1) continue;

        // Write the stuff
        cout << "Group: " << iterMAC->groupID << endl << "MAC: " << iterMAC->address.c_str() << endl;

        // List all IPs and write them
        for(list<ipAddressesStruc>::iterator ip = iterMAC->ipAddresses.begin(); ip != iterMAC->ipAddresses.end(); ip++){
            if (!ip->ipv6Predicate) cout << "IPv4: " << ip->address.c_str() <<  "\n";
            else cout << "IPv6: " << ip->address.c_str() <<  "\n";
        }
    }
}
