/**
 * This is the header file for the XML production
 * @class XML
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#ifndef PDS_CHOOSER_XML_H
#define PDS_CHOOSER_XML_H

/** Class definition **/
class XML {
public:
    ~XML(void);
    XML (list<macAddressesStruc> askerOutput, string outputFile);
    void produceXML();
    string error;
private:
    list<macAddressesStruc> resStructure;
    ofstream xmlFile;
    string outputFile;
    bool openOutputFile();
};

#endif
