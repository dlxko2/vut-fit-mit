/**
 * This class implements the XML production functionality
 * @class XML
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#include "Helper.h"
#include "XML.h"

/**
 * This is the constructor function
 * @param askerOutput - the asker special structure
 * @param outputFile - the output file name
 */
XML::XML(list <macAddressesStruc> askerOutput, string outputFile) {

    this->resStructure = askerOutput;
    this->outputFile = outputFile;
    if (!this->openOutputFile()) return ;
    this->produceXML();
}

/** This function opens the file **/
bool XML::openOutputFile() {

    // Try to open the file and check
    this->xmlFile.open (this->outputFile);
    if ( (this->xmlFile.rdstate() & std::ifstream::failbit ) != 0 ) {
        this->error = "Error opening the output file: " + this->outputFile;
        return false;
    }

    // The right result
    return true;
}

/** This is the destructor function **/
XML::~XML(void) {
    this->xmlFile.close();
}

/** The core function to generate the XML file  **/
void XML::produceXML() {

    // Create the XML header
    this->xmlFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    this->xmlFile << "<devices>\n";

    // List all MAC addresses
    for(list<macAddressesStruc>::iterator mac = this->resStructure.begin(); mac != this->resStructure.end(); mac++){

        // Write the start information
        this->xmlFile << "\t<host mac=\"" << mac->address.c_str() <<  "\"";

        // If selected append the group attribute
        if (mac->groupID != -1) this->xmlFile << " group=\"victim-pair-" << mac->groupID << "\">\n";
        else this->xmlFile << ">\n";

        // List all IP addresses
        for(list<ipAddressesStruc>::iterator ip = mac->ipAddresses.begin(); ip != mac->ipAddresses.end(); ip++){
            if (!ip->ipv6Predicate) this->xmlFile << "\t\t<ipv4>" << ip->address.c_str() <<  "</ipv4>\n";
            else this->xmlFile << "\t\t<ipv6>" << ip->address.c_str() <<  "</ipv6>\n";
        }

        // Finish element host
        this->xmlFile << "\t</host>\n";
    }

    // Finish element devices
    this->xmlFile << "</devices>";
}