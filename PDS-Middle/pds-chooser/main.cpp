/**
 * This is the main class
 * @class Main
 * @author Martin Pavelka
 * @date 16.04.2017
 */

#include "Helper.h"
#include "Args.h"
#include "Parser.h"
#include "Asker.h"
#include "XML.h"

/**
 * Function to handle the signals
 * @param signo - the signal number
 */
void sig_handler(int signo) {

    if (signo == SIGINT || signo == SIGUSR1) {
        printf("Signal received!\n");
        exit(EXIT_SUCCESS);
    }
}

/** Function to initialize the signal handlers **/
void initializeSignals() {

    if (signal(SIGINT, sig_handler) == SIG_ERR)
        cerr << "[SIGINT]" << "Can't initialize the signals" << endl;
    if (signal(SIGUSR1, sig_handler) == SIG_ERR)
        cerr << "[SIGUSR1]" << "Can't initialize the signals" << endl;
}

/**
 * The main function of program
 * @param argc - the number of arguments
 * @param argv - the array of arguments
 * @return - the integer value
 */
int main(int argc, char *argv[]) {

    // Initialize signals
    initializeSignals();

    // Get arguments - wrong argument is not exception
    if (DEBUG) cout << "Parsing the arguments" << endl;
    Args mainArgs(argc, argv);
    if (mainArgs.error.length() > 0) {
        cerr << mainArgs.error << endl;
        return EXIT_FAILURE;
    }

    // Parse input file
    if (DEBUG) cout << "Parsing the XML" << endl;
    Parser mainParser(mainArgs.inputFile);
    if (mainParser.error.length() > 0) {
        cerr << mainParser.error << endl;
        return EXIT_FAILURE;
    }

    // Ask for victims
    if (DEBUG) cout << "Starting asker" << endl;
    Asker mainAsker(mainParser.parserOutput);

    // Produce output XML file
    if (DEBUG) cout << "Generating the XML" << endl;
    XML mainXML(mainAsker.askerOutput, mainArgs.outputFile);
    if (mainXML.error.length() > 0) {
        cerr << mainParser.error << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}