/**
 * The main header file
 * @class Helpers
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#ifndef PDS_SCANNER_HELPERS_H
#define PDS_SCANNER_HELPERS_H

#include <string>
#include <iostream>
#include <cstdint>
#include <list>
#include <cstring>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fstream>

#define DEBUG 1
using namespace std;

/** Structure for IP addresses **/
struct  ipAddressesStruc {
    bool ipv6Predicate;
    string address;
};

/** Structure of list for XML output **/
struct macAddressesStruc{
    int index;
    int groupID;
    string address;
    list<ipAddressesStruc> ipAddresses;
};

#endif
