/**
 * This is the header for the parser
 * @class Parser
 * @author Martin Pavelka
 * @date 16.04.2017
 */

#ifndef PDS_CHOOSER_PARSER_H
#define PDS_CHOOSER_PARSER_H

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>

class Parser {
public:
    Parser(string inputFile);
    list<macAddressesStruc> parserOutput;
    string error;
private:
    void parseInput(xmlNode *rootElement);
};

#endif
