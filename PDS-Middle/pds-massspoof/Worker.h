/**
 * The worker header file
 * @class Worker
 * @author Martin Pavelka
 * @date 22.04.2017
 */

#ifndef PDS_MASSSPOOF_WORKER_H
#define PDS_MASSSPOOF_WORKER_H


class Worker {
public:

    Worker();
    pthread_t *threads;
    int threadsNumber;
    list<macAddressesStruc> parsedData;
    void startWorker(list <macAddressesStruc> parsedData, Args arguments);

private:

    string interface;
    int interval;
    string protocol;
    string appLocation;
    void doSpoofToVictims();
    list<sendListStruct> sendList;
    static void *startSpoofer(void *arg);
    void createThread(string ip1, string mac1, string ip2, string mac2);
};


#endif
