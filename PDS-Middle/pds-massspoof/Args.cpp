/**
 * This class implements the arguments functionality
 * @class Args
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#include "Helper.h"
#include "Args.h"

/**
 * Constructor function
 * @brief initialize the arguments variables and set new ones
 * @param argc - the number of arguments
 * @param argv - the array of arguments
 */
Args::Args(int argc, char *argv[]) {

    // Initialize the structure and option variable
    int option = 0;
    this->appLocation = "";

    // Check if we have the correct number of arguments
    if (argc < 9) {
        this->help();
        this->error = "Args::Args - wrong number of arguments";
        return;
    }

    // Use getopt to get the -i and -f arguments
    while ((option = getopt(argc, argv,"a:i:t:p:f:")) != -1) {
        switch (option) {
            case 'a' : this->appLocation = optarg; break;
            case 'i' : this->interface  = optarg; break;
            case 't' : this->interval  = atoi(optarg); break;
            case 'p' : this->protocol  = optarg; break;
            case 'f' : this->inputFile  = optarg; break;
            case ':': this->help(); this->error = "Args::Args - option requires argument";return;
            case '?': this->help(); this->error = "Args::Args - wrong argument used";return;
            default: this->help();  this->error = "Args::Args - wrong argument used";return;
        }
    }

}

/**
 * Function to print the help page
 * @brief on problem with arguments write this stuff
 */
void Args::help() {

    cout << "#Author: Martin Pavelka" << endl;
    cout << "#Modified: 13.04.2017" << endl;
    cout << "#Param1: i - interface" << endl;
    cout << "#Param2: t - interval" << endl;
    cout << "#Param3: p - protocol" << endl;
    cout << "#Param4: f - input file" << endl;
    cout << "#Run: ./pds-massspoof -i [interface] -t [interval] -p [protocol] -f [file]" << endl;
}
