/**
 * This class implements the arguments functionality
 * @class Args
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#ifndef PDS_SCANNER_ARGUMENTS_H
#define PDS_SCANNER_ARGUMENTS_H

#include <getopt.h>

/** Class definition */
class Args {
public:
    Args(int argc, char *argv[]);
    string inputFile;
    string protocol;
    string interface;
    string error;
    string appLocation;
    int interval;

private:
    void help();
};

#endif
