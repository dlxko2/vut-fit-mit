/**
 * This class implements helpers functions to cover the minor functions
 * They are used in the whole project and covers network operations
 * @class Helpers
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#ifndef PDS_SCANNER_HELPERS_H
#define PDS_SCANNER_HELPERS_H

#include <string>
#include <iostream>
#include <cstdint>
#include <list>
#include <cstring>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fstream>

#define DEBUG 1

using namespace std;

/** Structure for IP addresses **/
struct  ipAddressesStruc {
    pthread_t createdThread;
    bool ipv6Predicate;
    string address;
};

/** Structure of list for XML output **/
struct macAddressesStruc{
    int groupID;
    string address;
    list<ipAddressesStruc> ipAddresses;
};

/** Structure for ip used **/
struct sendListStruct{
    string ipOne;
    string ipTwo;
};

#endif
