/**
 * The whole massproof functionality
 * @class Worker
 * @author Martin Pavelka
 * @date 22.04.2017
 */

#include "Helper.h"
#include "Args.h"
#include "Worker.h"

Worker::Worker() {}

/**
 * Initialize the class
 * @param parsedData - parsed data from xml
 * @param arguments - the arguments
 */
void Worker::startWorker(list<macAddressesStruc> parsedData, Args arguments) {

    this->threadsNumber = 0;
    this->threads = NULL;
    this->parsedData = parsedData;
    this->interface = arguments.interface;
    this->interval = arguments.interval;
    this->protocol = arguments.protocol;
    this->appLocation = arguments.appLocation;
    this->doSpoofToVictims();
}

/** Find all combinations **/
void Worker::doSpoofToVictims() {

    // Init
    int found = 0;
    int actualGroup = 1;
    bool groupFound = true;

    // While any group
    while (groupFound) {

        // Find the first MAC owner
        groupFound = false;
        for (list<macAddressesStruc>::iterator mac = parsedData.begin(); mac != parsedData.end(); mac++) {
            string oneMAC = mac->address;

            // If not victim group skip
            if (mac->groupID != actualGroup) continue;
            else groupFound = true;

            // Find the second MAC owner
            for (list<macAddressesStruc>::iterator mac2 = parsedData.begin(); mac2 != parsedData.end(); mac2++) {
                string twoMAC = mac2->address;

                // If not victim group skip
                if (mac2->groupID != actualGroup || mac2->address.compare(mac->address) == 0) continue;
                else groupFound = true;

                // From all IP from first MAC
                for (list<ipAddressesStruc>::iterator ip = mac->ipAddresses.begin(); ip != mac->ipAddresses.end(); ip++) {
                    string oneIP = ip->address;

                    // Remove non correct IPs
                    if (this->protocol.compare("arp") == 0 && ip->ipv6Predicate) continue;
                    if (this->protocol.compare("ndp") == 0 && !ip->ipv6Predicate) continue;

                    // For all IP from second MAC
                    for (list<ipAddressesStruc>::iterator ip2 = mac2->ipAddresses.begin(); ip2 != mac2->ipAddresses.end(); ip2++) {
                        string twoIP = ip2->address;

                        // Just the same protocol
                        if (ip->ipv6Predicate != ip2->ipv6Predicate) continue;
                        if (ip->address.compare(ip2->address) == 0) continue;

                        // Check if already done
                        bool alreadyDone = false;
                        for (list<sendListStruct>::iterator done = sendList.begin(); done != sendList.end(); done++) {
                            if (done->ipOne == ip->address && done->ipTwo == ip2->address) alreadyDone = true;
                            if (done->ipTwo == ip->address && done->ipOne == ip2->address) alreadyDone = true;
                        }

                        if (alreadyDone) continue;
                        this->createThread(ip2->address, mac2->address, ip->address, mac->address);
                    }
                }
            }

            // Increase the group
            ++found;
            if (found > 1) {
                ++actualGroup;
                found = 0;
            }
        }
    }
}

/**
 * Create the thread
 * @param ip1 - the victim one ip
 * @param mac1 - the victim one MAC
 * @param ip2 - the victim two ip
 * @param mac2 - the victim two MAC
 */
void Worker::createThread(string ip1, string mac1, string ip2, string mac2) {

    // Create the parameters
    string params = " -i " + this->interface + " -t " + to_string(this->interval) + " -p " +
                    this->protocol + " -victim1ip " + ip1 + " -victim1mac " + mac1+
                    " -victim2ip " + ip2 + " -victim2mac " + mac2;

    // Create the command
    string command;
    if (this->appLocation.length() > 0) command = this->appLocation + params;
    else command = "./pds-spoof" + params;

    // Save as send
    sendListStruct node;
    node.ipOne = ip1;
    node.ipTwo = ip2;
    sendList.push_front(node);

    // Create thread
    pthread_t threadPoint;
    pthread_create(&threadPoint, NULL, this->startSpoofer, (void *) command.c_str());

    // Save pointer
    this->threads = (pthread_t *) realloc(this->threads, this->threadsNumber * sizeof(pthread_t) + 1);
    this->threads[threadsNumber] = threadPoint;
    ++threadsNumber;
    sleep(2);
}

/**
 * Start the command
 * @param arg - the command
 */
void * Worker::startSpoofer(void *arg) {

    char *command = (char *) arg;
    cout << command << endl;
    if (system(command) == -1)
        cerr << "Problem while executing the command" << endl;
    pthread_exit(EXIT_SUCCESS);
}
