/**
 * This is the arguments header
 * @class Args
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#ifndef PDS_SCANNER_ARGUMENTS_H
#define PDS_SCANNER_ARGUMENTS_H

/** Class definition */
class Args {
public:
    Args(int argc, char *argv[]);
    argumentsStruct getStructFromObject();
    string error;
private:
    string interface;
    string protocol;
    string victim1IP;
    string victim1MAC;
    string victim2IP;
    string victim2MAC;
    int interval;
    void help();
};

#endif
