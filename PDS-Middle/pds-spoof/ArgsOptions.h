/**
 * This is the arguments options header
 * @class Args
 * @author Martin Pavelka
 * @date 22.04.2017
 */

#ifndef PDS_SPOOFER_ARGSOPTIONS_H_H
#define PDS_SPOOFER_ARGSOPTIONS_H_H

#include <getopt.h>

/** Long options array */
static struct option longOptions[] = {
    {"victim1ip",   required_argument,  0, 'l'},
    {"victim1mac",  required_argument,  0, 'm'},
    {"victim2ip",   required_argument,  0, 'n'},
    {"victim2mac",  required_argument,  0, 'o'},
    {"gratuitous",  no_argument,        0, 'g'},
    {0,0,0,0},
};

#endif
