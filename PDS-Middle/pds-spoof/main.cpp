/**
 * The main function of pds-scanner
 * Created by Martin Pavelka
 * 15.04.2017
 */

using namespace std;

#include "Helpers.h"
#include "Args.h"
#include "Initializer.h"
#include "Arp.h"
#include "Ndp.h"

// Global variables
argumentsStruct globalArgs;
bool globalArp;

/**
 * Function to handle the signals
 * @param signo - the signal number
 */
void sig_handler(int signo) {

    if (signo == SIGINT || signo == SIGUSR1) {

        // Print and wait
        printf("Received signal, healing the cache!\n");
        sleep(EXIT_WAIT_TIMER);

        // ARP and NDP
        if (globalArp) {
            Arp mainArp(globalArgs);
            mainArp.StartHeal();
        } else {
            Ndp mainNdp(globalArgs);
            mainNdp.StartHeal();
        }

        // EXIT
        exit(EXIT_SUCCESS);
    }
}

/** Function to initialize the signal handlers **/
void initializeSignals() {

    if (signal(SIGINT, sig_handler) == SIG_ERR) cerr << "[SIGS]" << "Can't initialize the signals" << endl;
    if (signal(SIGUSR1, sig_handler) == SIG_ERR) cerr << "[SIGS]" << "Can't initialize the signals" << endl;
}

/**
 * The main function of program
 * @param argc - the number of arguments
 * @param argv - the array of arguments
 * @return - the integer value
 */
int main(int argc, char *argv[]) {

    // Initialize the signals
    initializeSignals();

    // Get arguments
    if (DEBUG) cout << "Parsing the arguments" << endl;
    Args mainArgs(argc, argv);
    globalArgs = mainArgs.getStructFromObject();
    if (mainArgs.error.length() > 0) {
        cout << mainArgs.error << endl;
        return EXIT_FAILURE;
    }

    // Process the ARP
    if (globalArgs.protocol.compare("arp") == 0) {
        if (DEBUG) cout << "Spoofing with ARP" << endl;
        globalArp = true;
        Arp mainArp(globalArgs);
        mainArp.StartPoisoning();
    }

    // Process the NDP
    if (globalArgs.protocol.compare("ndp") == 0) {
        if (DEBUG) cout << "Spoofing the NDP" << endl;
        globalArp = false;
        Ndp mainNdp(globalArgs);
        mainNdp.StartPoisoning();
    }

    return EXIT_SUCCESS;
}