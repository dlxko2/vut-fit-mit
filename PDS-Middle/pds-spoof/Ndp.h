/**
 * This class implements the NDP functionality
 * @class Ndp
 * @author Martin Pavelka
 * @date 15.04.2017
 */

#ifndef PDS_SCANNER_NDP_H
#define PDS_SCANNER_NDP_H

#include <netinet/in.h>
#include <netinet/icmp6.h>
#include <netinet/ip.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>

/** Class definition **/
class Ndp {
public:

    Ndp(argumentsStruct arguments);
    void StartPoisoning();
    void StartHeal();
    virtual ~Ndp();
    bool error;

private:

    // Variables
    struct sockaddr_ll device;
    int ndpSocket;
    Initializer ndpInitializer;

    // Internal functions
    void lazySleeper();
    void initializeSocket();
    void initializeDevice();
    uint16_t checksum(uint16_t *addr, int len);
    void sendThePacket(char *packet, size_t size);
    packetStruct createPacket(uint8_t *v1mac, uint8_t *v1ip, uint8_t *v2mac, uint8_t *v2ip, uint8_t *myMac);
    packetStruct createPingPacket(uint8_t *v1mac, uint8_t *v1ip, uint8_t *v2mac, uint8_t *v2ip);
};

#endif
