/**
 * This class implements the initializer functions
 * They are used in the project start to set the important parameters
 * @class Initializer
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#ifndef PDS_SCANNER_INITIALIZER_H
#define PDS_SCANNER_INITIALIZER_H

#include <arpa/inet.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <regex>

using namespace std;

/** Class definition **/
class Initializer {
public:

    // Base functions
    Initializer(argumentsStruct arguments);
    ~Initializer(void);
    void InitIPv4();
    void InitIPv6();
    bool error;

    // Variables
    uint8_t *srcMac;
    uint8_t victim1MacConv[MAC_ADDRESS_SIZE];
    uint8_t victim2MacConv[MAC_ADDRESS_SIZE];
    uint8_t victim1IpConv[IP_ADDRESS_SIZE];
    uint8_t victim2IpConv[IP_ADDRESS_SIZE];
    uint8_t victim1Ip6Conv[IP6_ADDRESS_SIZE];
    uint8_t victim2Ip6Conv[IP6_ADDRESS_SIZE];
    struct timespec lazyInterval;
    string interface;

private:

    void InitializeLazySleep();
    void getVictim1IpConverted();
    void getVictim2IpConverted();
    void getVictim1Ip6Converted();
    void getVictim2Ip6Converted();
    void convertVictim1MAC();
    void convertVictim2MAC();
    void getSourceMAC();
    string victim1IP;
    string victim2IP;
    string victim1MAC;
    string victim2MAC;
    int interval;
    char *srcIp;
};

#endif
