/**
 * This class implements the initializer functions
 * They are used in the project start to set the important parameters
 * @class Initializer
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#include "Helpers.h"
#include "Initializer.h"

/**
 * Constructor function to initialize MAC
 * @param arguments - the argument structure
 */
Initializer::Initializer(argumentsStruct arguments) {

    // Init from the arguments
    this->error = false;
    this->interface = arguments.interface;
    this->interval = arguments.interval;
    this->victim1IP = arguments.victim1IP;
    this->victim2IP = arguments.victim2IP;
    this->victim1MAC = arguments.victim1MAC;
    this->victim2MAC = arguments.victim2MAC;

    // Allocations
    this->srcMac = (uint8_t *) calloc(MAC_ADDRESS_SIZE, sizeof(uint8_t));
    if (this->srcMac == NULL) {
        cerr << "Initializer::Initializer - Can't allocate the memory" << endl;
        this->error = true;
        return;
    }

    // Get source mac
    try {
        this->getSourceMAC();
        this->InitializeLazySleep();
    }
    catch (const char* msg) {
        this->error = true;
        cerr << msg << endl;
    };
}

/** Initialize for IPv4 **/
void Initializer::InitIPv4() {

    // Allocations
    this->srcIp = (char *) calloc(INET_ADDRSTRLEN, sizeof(char));
    if (this->srcIp == NULL) {
        cerr << "Initializer::InitIPv4 - Can't allocate the memory" << endl;
        this->error = true;
        return;
    }

    // Initialize
    try {
        this->convertVictim1MAC();
        this->convertVictim2MAC();
        this->getVictim1IpConverted();
        this->getVictim2IpConverted();
    } catch (const char* msg) {
        this->error = true;
        cerr << msg << endl;
    };
}

/** Initialize for IPv6 **/
void Initializer::InitIPv6() {

    // Allocations
    this->srcIp = (char *) calloc(INET6_ADDRSTRLEN, sizeof(char));
    if (this->srcIp == NULL) {
        cerr << "Initializer::InitIPv6 - Can't allocate the memory" << endl;
        this->error = true;
        return;
    }

    // Initialize
    try {
        this->convertVictim1MAC();
        this->convertVictim2MAC();
        this->getVictim1Ip6Converted();
        this->getVictim2Ip6Converted();
    }
    catch (const char* msg) {
        this->error = true;
        cerr << msg << endl;
    };
}

/** The destructor function **/
Initializer::~Initializer(void) {

}

/** Init sleep timer **/
void Initializer::InitializeLazySleep() {

    // Divide
    int seconds = interval / 1000;
    int extend = interval % 1000;

    // Initialize sleeper structure
    this->lazyInterval.tv_sec = seconds;
    this->lazyInterval.tv_nsec = extend*1000*1000;
}

/** Get the source MAC address for the selected interface **/
void Initializer::getSourceMAC() {

    // Try to create the socket
    int sd;
    if ((sd = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0)
        throw "Initializer::getSourceMAC - Can't create the socket!";

    // Use ioctl to look up interface name and get its MAC address.
    struct ifreq ifr;
    memset (&ifr, 0, sizeof (ifr));
    snprintf (ifr.ifr_name, sizeof (ifr.ifr_name), "%s", this->interface.c_str());
    if (ioctl (sd, SIOCGIFHWADDR, &ifr) < 0) {
        close(sd);
        throw "Initializer::getSourceMAC - Can't get the MAC";
    }

    // Save the MAC address
    memcpy (this->srcMac, ifr.ifr_hwaddr.sa_data, 6 * sizeof (uint8_t));
    close (sd);
}

/** Convert the victim one MAC into binary **/
void Initializer::convertVictim1MAC() {

    unsigned int values[6];
    int i;

    if( 6 == sscanf( this->victim1MAC.c_str(), "%2x%2x.%2x%2x.%2x%2x", &values[0], &values[1], &values[2], &values[3], &values[4], &values[5]))
        for( i = 0; i < 6; ++i ) this->victim1MacConv[i] = (char) values[i];
    else
        throw "Initializer::convertVictim1MAC - can't convert MAC";
}

/** Convert the victim two MAC into binary **/
void Initializer::convertVictim2MAC() {

    unsigned int values[6];
    int i;

    if( 6 == sscanf( this->victim2MAC.c_str(), "%2x%2x.%2x%2x.%2x%2x", &values[0], &values[1], &values[2], &values[3], &values[4], &values[5]))
        for( i = 0; i < 6; ++i ) this->victim2MacConv[i] = (char) values[i];
    else
        throw "Initializer::convertVictim2MAC - can't convert MAC";
}

/** Function to convert the ipv6 for victim 1 **/
void Initializer::getVictim1Ip6Converted() {
    if ((inet_pton (AF_INET6, this->victim2IP.c_str(), &this->victim2Ip6Conv)) != 1)
        throw "Initializer::getVictim1Ip6Converted - Can't convert the victim 2 IP";
}

/** Function to convert the ipv6 for victim 2 **/
void Initializer::getVictim2Ip6Converted() {
    if ((inet_pton (AF_INET6, this->victim1IP.c_str(), &this->victim1Ip6Conv)) != 1)
        throw "Initializer::getVictim2Ip6Converted - Can't convert the victim 1 IP";
}

/** Convert the victim 1 IP from the string to binary */
void Initializer::getVictim1IpConverted() {
    if ((inet_pton (AF_INET, this->victim1IP.c_str(), &this->victim1IpConv)) != 1)
        throw "Initializer::getIPVictim1IpConverted - Can't convert the victim 1 IP";
}

/** Convert the victim 2 IP from the string to binary */
void Initializer::getVictim2IpConverted() {
    if ((inet_pton (AF_INET, this->victim2IP.c_str(), &this->victim2IpConv)) != 1)
        throw "Initializer::getIPVictim1IpConverted - Can't convert the victim 2 IP";
}
