/**
 * This class implements the ARP functionality
 * @class Arp
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#ifndef PDS_SCANNER_ARP_H
#define PDS_SCANNER_ARP_H

#include <netdb.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <net/if.h>

/** Class definition */
class Arp {
public:

    Arp(argumentsStruct arguments);
    ~Arp();
    bool error;
    void StartPoisoning();
    void StartHeal();

private:

    // Storage for connection
    int arpSocket;
    struct sockaddr_ll device;
    Initializer arpInitializer;

    // Internal functions
    void lazySleeper();
    void initializeDevice();
    void initializeSocket();
    packetStruct createPacket(uint8_t *v1mac, uint8_t *v1ip, uint8_t *v2ip, uint8_t *myMac);
    void sendThePacket(char *packet, size_t size);
};

#endif
