/**
 * This class implements the arguments functionality
 * @class Args
 * @author Martin Pavelka
 * @date 13.04.2017
 */

#include "Helpers.h"
#include "Args.h"
#include "ArgsOptions.h"

/**
 * Constructor function
 * @brief initialize the arguments variables and set new ones
 * @param argc - the number of arguments
 * @param argv - the array of arguments
 */
Args::Args(int argc, char *argv[]) {

    // Initialize the structure and option variable
    int option = 0;
    int index = 0;
    extern int opterr;
    bool gratuitous = false;
    opterr = 0;

    // Check if we have the correct number of arguments
    if (argc < 15) {
        this->help();
        this->error = "Args::Args - wrong number of arguments";
        return;
    }

    // Use getopt to get the -i and -f arguments
    while ((option = getopt_long_only(argc, argv,"i:t:p:l:m:n:og", longOptions,&index)) != -1) {
        switch (option) {
            case 'i' : this->interface  = optarg; break;
            case 'p' : this->protocol   = optarg; break;
            case 't' : this->interval   = atoi(optarg); break;
            case 'l' : this->victim1IP  = optarg; break;
            case 'm' : this->victim1MAC = optarg; break;
            case 'n' : this->victim2IP  = optarg; break;
            case 'o' : this->victim2MAC = optarg; break;
            case 'g' : gratuitous = true; break;
            case ':': this->help(); this->error = "Args::Args - option requires argument";return;
            case '?': this->help(); this->error = "Args::Args - wrong argument used";return;
            default: this->help();  this->error = "Args::Args - wrong argument used";return;
        }
    }

    // If gratuitous use this parameters :D
    if (gratuitous) {
        this->victim1IP = "10.10.0.255";
        this->victim1MAC = "ffff.ffff.ffff";
    }
}

/**
 * Function to print the help page
 * @brief on problem with arguments write this stuff
 */
void Args::help() {

    cout << "#Author: Martin Pavelka" << endl;
    cout << "#Modified: 13.04.2017" << endl;
    cout << "#Param1: i - the name of the interface ex. eth0" << endl;
    cout << "#Param2: t - time in miliseconds to send responds" << endl;
    cout << "#Param3: p - protocol arp or ndp" << endl;
    cout << "#Param4: victim1ip - target ip" << endl;
    cout << "#Param5: victim1mac - target MAC" << endl;
    cout << "#Param6: victim2ip - gateway ip" << endl;
    cout << "#Param7: victim2mac - gateway MAC" << endl;
    cout <<
        "#Run: ./arguments -i [itf] -t [time] -p [protocol] -victim1ip [ip]" << endl
        << " -victim1mac [mac] -victim2ip [ip] -victim2mac [mac]" << endl;
}

argumentsStruct Args::getStructFromObject() {

    argumentsStruct output;
    output.interval = this->interval;
    output.interface = this->interface;
    output.victim1IP = this->victim1IP;
    output.victim2IP = this->victim2IP;
    output.victim1MAC = this->victim1MAC;
    output.victim2MAC = this->victim2MAC;
    output.protocol = this->protocol;
    return output;
}