/**
 * This class implements the NDP functionality
 * @class Ndp
 * @author Martin Pavelka
 * @date 16.04.2017
 */

#include <netinet/in.h>
#include <netinet/icmp6.h>
#include <linux/if_ether.h>
#include <net/if.h>
#include <linux/if_packet.h>
#include <unistd.h>
#include "Helpers.h"
#include "Initializer.h"
#include "Ndp.h"

/**
 * Constructor function to create the NDP packet
 * @param ifParam - the interface name
 */
Ndp::Ndp (std::string ifParam) : ndpInitializer(ifParam) {

    // Initialize interface
    this->interface = ifParam;

    // Run initialization
    ndpInitializer.Initialize6();
    if (ndpInitializer.error) {
        cerr << "Ndp::Ndp - Skipping NDP because of initializer error!" << endl;
        return;
    }

    // Initialize time structure
    struct timespec sleepTimer;
    sleepTimer.tv_sec = 0;
    sleepTimer.tv_nsec = NDP_SEND_WAIT_TIMER;

    // Send packet ping
    try {
        this->createPacketPing(false);
        nanosleep(&sleepTimer, NULL);
        this->createPacketPing(true);
    }
    catch (const char* msg) {
        cerr << msg << endl;
    };
}

/** Function to create the NDP packet **/
void Ndp::createPacketPing(bool routers) {

    // Create the ethernet header
    ethhdr ethernet;
    memcpy(ethernet.h_dest, this->ndpInitializer.srcMulticastMAC, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    memcpy(ethernet.h_source, this->ndpInitializer.srcMac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    ethernet.h_proto = htons(ETH_P_IPV6) ;

    // Create the IPv6 header
    ipv6hdr ipv6;
    ipv6.hop_limit = IPV6_HOP_MAX;
    ipv6.version = IPV6_VERSION;
    ipv6.nexthdr = IPV6_ICMP_NEXT;
    ipv6.priority = ZERO;
    ipv6.payload_len = htons(ICMP_PING_HEADER_SIZE);
    memcpy(&ipv6.saddr, this->ndpInitializer.srcIp6Conv, IP6_ADDRESS_SIZE * sizeof(uint8_t));
    if (!routers) memcpy(&ipv6.daddr, this->ndpInitializer.dstMulticastIP, IP6_ADDRESS_SIZE * sizeof(uint8_t));
    else memcpy(&ipv6.daddr, this->ndpInitializer.dstMulticastRouterIP, IP6_ADDRESS_SIZE * sizeof(uint8_t));
    memset(&ipv6.flow_lbl, ZERO, sizeof(ipv6.flow_lbl));

    // Create the NDP header
    nd_neighbor_advert ndp;
    ndp.nd_na_hdr.icmp6_type = ICMP6_ECHO_REQUEST;
    ndp.nd_na_hdr.icmp6_code = ZERO;
    ndp.nd_na_hdr.icmp6_cksum = htons(ZERO);

    // Initialize pseudo header structures
    char *pseudoHeader = (char *) calloc(MAXIMUM_PACKET_SIZE, sizeof(char));
    int pseudoLength = (IP6_ADDRESS_SIZE*2) + PSEUDO_HEADER_INFO + ICMP_PING_HEADER_SIZE;

    // Create the pseudo header
    pseudoHeader[32] = ZERO;
    pseudoHeader[33] = ZERO;
    pseudoHeader[34] = (ICMP_PING_HEADER_SIZE) / 256;
    pseudoHeader[35] = (ICMP_PING_HEADER_SIZE) % 256;
    pseudoHeader[36] = ZERO;
    pseudoHeader[37] = ZERO;
    pseudoHeader[38] = ZERO;
    pseudoHeader[39] = IPPROTO_ICMPV6;
    if (!routers) memcpy (pseudoHeader, this->ndpInitializer.dstMulticastIP, IP6_ADDRESS_SIZE * sizeof (uint8_t));
    else memcpy (pseudoHeader, this->ndpInitializer.dstMulticastRouterIP, IP6_ADDRESS_SIZE * sizeof (uint8_t));
    memcpy (pseudoHeader+IP6_ADDRESS_SIZE, this->ndpInitializer.srcIp6Conv, IP6_ADDRESS_SIZE * sizeof (uint8_t));
    memcpy(pseudoHeader + PSEUDO_HEADER_SIZE, &ndp, ICMP_PING_HEADER_SIZE * sizeof(uint8_t));
    ndp.nd_na_hdr.icmp6_cksum = calculateChecksum ((uint16_t *) pseudoHeader, pseudoLength);

    // Create the output packet
    size_t resLength = ETHERNET_HEADER_SIZE + IPV6_HEADER_SIZE + ICMP_PING_HEADER_SIZE;
    char *res = (char *) calloc(MAXIMUM_PACKET_SIZE, sizeof(char));
    memcpy(res, &ethernet, ETHERNET_HEADER_SIZE * sizeof(uint8_t));
    memcpy(res + ETHERNET_HEADER_SIZE, &ipv6, IPV6_HEADER_SIZE * sizeof(uint8_t));
    memcpy(res + ETHERNET_HEADER_SIZE + IPV6_HEADER_SIZE, &ndp, ICMP_PING_HEADER_SIZE * sizeof(uint8_t));

    // Open the socket
    int sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if(sock < ZERO) throw "Ndp::createPacketPing - Can't create socket";

    // Initialize the device
    struct sockaddr_ll device;
    memset(&device, ZERO, sizeof(device));
    device.sll_family = AF_PACKET;
    device.sll_halen = MAC_ADDRESS_SIZE;
    memcpy(device.sll_addr, this->ndpInitializer.srcMac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    if ((device.sll_ifindex = if_nametoindex(this->interface.c_str())) == ZERO)
        throw "Ndp::createPacketPing - Can't get index of the device";

    // Send the packet
    if ((sendto(sock, res, resLength, ZERO, (struct sockaddr *) &device, sizeof(device))) <= 0)
        throw  "Ndp::createPacketPing - Can't send the packet";

    // Close the socket and dealloc
    close(sock);
    free(pseudoHeader);
}

/**
 * Function to calculate the ICMPv6 checksum
 * @param addr - the pseudo header pointer
 * @param len - the pseudo header length
 * @return - the checksum result
 */
uint16_t Ndp::calculateChecksum (uint16_t *address, int len) {

    // Initialize the variables
    int count = len;
    register uint32_t sum = 0;
    uint16_t answer = 0;

    // 2 bytes summing
    while (count > 1) {
        sum += *(address++);
        count -= 2;
    }

    // If any byte missing add it
    if (count > 0) sum += *(uint8_t *) address;

    // 32 -> 16 bits
    while (sum >> 16) sum = (sum & 0xffff) + (sum >> 16);

    // Checksum is the complement
    answer = (uint16_t) ~sum;
    return (answer);
}
