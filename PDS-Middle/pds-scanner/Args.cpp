/**
 * This class implements the arguments functionality
 * @class Args
 * @author Martin Pavelka
 * @date 22.02.2017
 */

#include "Helpers.h"
#include "Args.h"

/**
 * Constructor function
 * @brief initialize the arguments variables and set new ones
 * @param argc - the number of arguments
 * @param argv - the array of arguments
 */
Args::Args(int argc, char *argv[]) {

    // Initialize the structure and option variable
    int option = 0;

    // Check if we have the correct number of arguments
    if (argc != 5) {
        this->error = "Wrong number of arguments";
        help();
        return;
    }

    // Use getopt to get the -i and -f arguments
    while ((option = getopt(argc, argv,"i:f:")) != -1) {
        switch (option) {
            case 'i' : this->interface = optarg; break;
            case 'f' : this->file = optarg; break;
            default: help(); this->error = "Wrong argument used"; break;
        }
    }
}

/** Function to print the help page **/
void Args::help() {

    cout << "#Author: Martin Pavelka" << endl;
    cout << "#Modified: 18.02.2017" << endl;
    cout << "#Param1: i - the name of the interface ex. eth0" << endl;
    cout << "#Param2: f - the output xml file ex. out.xml" << endl;
    cout << "#Run: ./arguments -i eh0 -f out.xml" << endl;
}
