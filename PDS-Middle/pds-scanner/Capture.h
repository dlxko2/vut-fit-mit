/**
 * This class implements the PCAP functionality
 * @class Capture
 * @author Martin Pavelka
 * @date 26.02.2017
 */

#ifndef PDS_SCANNER_CAPTURE_H
#define PDS_SCANNER_CAPTURE_H

#include <arpa/inet.h>

#include <cstdio>
#include <pcap.h>

#include <string>
#include <iostream>

#include <linux/if_ether.h>

class Capture {
public:
    Capture();
    void initObject(string interface, string srcMAC);
    list<macAddressesStruc> getCaptureList();
    pthread_t threadPtr;

private:
    static void * captureArp(void *arg);
    static void my_packet_handler(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
};

#endif
