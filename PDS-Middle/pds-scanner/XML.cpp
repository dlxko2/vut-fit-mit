/**
 * This class implements the XML functionality
 * @class XML
 * @author Martin Pavelka
 * @date 26.02.2017
 */

#include "XML.h"

/**
 * Constructor function to initialize the instance variables
 * @brief initialization and generating the output XML
 * @param outputFile
 * @param captureList
 */
XML::XML(string outputFile, list<macAddressesStruc> captureList) {

    this->captureList = captureList;
    this->outputFile = outputFile;
    this->openOutputFile();
    this->produceXML();
}

/** This function opens the file **/
void XML::openOutputFile() {
    this->xmlFile.open (this->outputFile);
}

/** This is the destructor function **/
XML::~XML(void) {
    this->xmlFile.close();
}

/** The core function to generate the XML file **/
void XML::produceXML() {

    // Create the header
    this->xmlFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    this->xmlFile << "<devices>\n";

    // For all MAC
    for(list<macAddressesStruc>::iterator iterMAC = this->captureList.begin(); iterMAC != this->captureList.end(); iterMAC++){
        this->xmlFile << "\t<host mac=\"" << iterMAC->address.c_str() <<  "\">\n";

        // For all IPs
        for(list<ipAddressesStruc>::iterator iterIP = iterMAC->ipAddresses.begin(); iterIP != iterMAC->ipAddresses.end(); iterIP++){
            if (!iterIP->ipv6Predicate) this->xmlFile << "\t\t<ipv4>" << iterIP->address.c_str() <<  "</ipv4>\n";
            else this->xmlFile << "\t\t<ipv6>" << iterIP->address.c_str() <<  "</ipv6>\n";
        }

        // Finish element host
        this->xmlFile << "\t</host>\n";
    }

    // Finish element devices
    this->xmlFile << "</devices>";
}