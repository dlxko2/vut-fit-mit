/**
 * This class implements the initializer functions
 * They are used in the project start to set the important parameters
 * @class Initializer
 * @author Martin Pavelka
 * @date 16.04.2017
 */

#include "Helpers.h"
#include "Initializer.h"

/**
 * The constructor function
 * @param interface - the interface name
 */
Initializer::Initializer(string interface) {

    // Initialize the instance variables
    this->error = false;
    this->interface = interface;
    this->srcMask = NULL;
    this->srcIp = NULL;
    this->srcIp6 = NULL;
    this->srcMulticastMAC = NULL;
    this->srcMac = NULL;

    // Initialize for both
    this->srcMac = (uint8_t *) calloc(6, sizeof (uint8_t));
    if (this->srcMac == NULL) {
        cerr << "Initializer::Initializer - Can't allocate the memory" << endl;
        this->error = true;
        return;
    }

    // Get source mac
    try { this->getSourceMAC(); }
    catch (const char* msg) {
        cerr <<  msg << endl;
        this->error = true;
    };
}

/** Initialize IPv4 **/
void Initializer::Initialize4() {

    // Allocate
    this->srcMask = (char *) calloc(INET_ADDRSTRLEN, sizeof(char));
    this->srcIp = (char *) calloc(INET_ADDRSTRLEN, sizeof(char));

    // Check
    if (this->srcMask == NULL || this->srcIp == NULL) {
        cerr << "Initializer::Initialize4 - Can't allocate the memory" << endl;
        this->error = true;
        return;
    }

    // Check ipv4 support
    try {
        this->getSourceIP();
    } catch (const char* msg) {
        cerr <<  msg << endl;
        this->error = true;
        return;
    };

    // Init
    try {
        this->getSourceMask();
        this->getIPConverted();
        this->getHostsRange();
    } catch (const char* msg) {
        cerr <<  msg << endl;
        this->error = true;
    };
}

/** Initialize IPv6 **/
void Initializer::Initialize6() {

    // Allocate
    this->srcIp6 = (char *) calloc(INET6_ADDRSTRLEN, sizeof(char));
    this->srcMulticastMAC = (char *) calloc(MAC_ADDRESS_SIZE, sizeof(uint8_t));

    // Check
    if (this->srcIp6 == NULL || this->srcMulticastMAC == NULL) {
        cerr << "Initializer::Initialize6 - Can't allocate the memory" << endl;
        this->error = true;
        return;
    }

    // Check the ipv6 support
    try {
        this->getSourceIPv6();
    } catch (const char *msg) {
        cerr << msg << endl;
        this->error = true;
        return;
    }

    // Init
    try {
        this->getIP6Converted();
        this->getMulticastIP();
        this->getMulticastRouterIP();
        this->getSrcMulticastMAC();
    } catch (const char *msg) {
        cerr << msg << endl;
        this->error = true;
    }
}

/** The destructor function **/
Initializer::~Initializer(void) {

    if (this->srcMask != NULL) free(this->srcMask);
    if (this->srcIp != NULL) free(this->srcIp);
    if (this->srcIp6 != NULL) free(this->srcIp6);
    if (this->srcMulticastMAC != NULL) free(this->srcMulticastMAC);
    if (this->srcMac != NULL) free(this->srcMac);
}

/** Get the source IP for the selected interface **/
void Initializer::getSourceIP() {

    // Initialize the interface addresses structure
    struct ifaddrs * ifAddressStruct = NULL, * ifAddress = NULL;
    getifaddrs(&ifAddressStruct);
    bool found = false;

    // For all interfaces find the correct one
    for (ifAddress = ifAddressStruct; ifAddress != NULL; ifAddress = ifAddress->ifa_next) {

        // Lookup for IPv4 address
        if (strcmp(ifAddress->ifa_name, this->interface.c_str()) == 0 && ifAddress ->ifa_addr->sa_family == AF_INET) {
            void *addressPtr = &((struct sockaddr_in *) ifAddress->ifa_addr)->sin_addr;
            inet_ntop(AF_INET, addressPtr, this->srcIp, INET_ADDRSTRLEN);
            found = true;
        }
    }

    // Check if found
    if (!found) throw "Initializer::getSourceIP - No IPv4 IP found on interface!";
}

/** Get the source IP for the selected interface **/
void Initializer::getSourceIPv6() {

    // Initialize the interface addresses structure
    struct ifaddrs * ifAddressStruct = NULL, * ifAddress = NULL;
    getifaddrs(&ifAddressStruct);
    bool found = false;

    // For all interfaces find the correct one
    for (ifAddress = ifAddressStruct; ifAddress != NULL; ifAddress = ifAddress->ifa_next) {

        // Lookup for IPv6 address
         if (strcmp(ifAddress->ifa_name, this->interface.c_str()) == 0 && ifAddress->ifa_addr->sa_family == AF_INET6) {
             void *address6Ptr = &((struct sockaddr_in6 *) ifAddress->ifa_addr)->sin6_addr;
             inet_ntop(AF_INET6, address6Ptr, this->srcIp6, INET6_ADDRSTRLEN);
             found = true;
        }
    }

    // Check if found
    if (!found) throw "Initializer::getSourceIPv6 - No IPv6 address found for the interface!";
}

/** Get the source mask for the selected interface **/
void Initializer::getSourceMask() {

    // Initialize the interface addresses structure
    struct ifaddrs * ifAddressStruct = NULL, * ifAddress = NULL;
    getifaddrs(&ifAddressStruct);
    bool found = false;

    // For all interfaces find the correct one with IPv4
    for (ifAddress = ifAddressStruct; ifAddress != NULL; ifAddress = ifAddress->ifa_next) {
        if (strcmp(ifAddress->ifa_name, interface.c_str()) == 0 && ifAddress ->ifa_addr->sa_family == AF_INET) {
            void *mask_ptr = &((struct sockaddr_in *) ifAddress->ifa_netmask)->sin_addr;
            inet_ntop(AF_INET, mask_ptr, srcMask, INET_ADDRSTRLEN);
            found = true;
        }
    }

    // Check if found
    if (!found) throw "Initializer::getSourceMask - No Mask found for the interface!";
}

/** Get the source MAC address for the selected interface **/
void Initializer::getSourceMAC() {

    // Try to create the socket
    int sd;
    if ((sd = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0)
        throw "Initializer::getSourceMAC - Can't create the socket! Do you use sudo?";

    // Use ioctl to look up interface name and get its MAC address.
    struct ifreq ifr;
    memset (&ifr, 0, sizeof (ifr));
    snprintf (ifr.ifr_name, sizeof (ifr.ifr_name), "%s", this->interface.c_str());
    if (ioctl (sd, SIOCGIFHWADDR, &ifr) < 0) {
        close(sd);
        throw "Initializer::getSourceMAC - Can't get the source MAC!";
    }

    // Save the MAC address
    memcpy (this->srcMac, ifr.ifr_hwaddr.sa_data, 6 * sizeof (uint8_t));

    // Get MAC as string
    char strMAC[MAC_ADDRESS_STR_SIZE];
    sprintf(strMAC,"%02X:%02X:%02X:%02X:%02X:%02X",this->srcMac[0],this->srcMac[1],
            this->srcMac[2],this->srcMac[3],this->srcMac[4],this->srcMac[5]);

    // Save and finish
    myMacStr = string(strMAC);
    close (sd);
}

/** Function to convert the IPs from the string to binary **/
void Initializer::getIPConverted() {
    if ((inet_pton (AF_INET, this->srcIp, &srcIpConv)) != 1)
        throw "Initializer::getIPConverted - Can't convert the source IPv4!";
}

/** Function to convert the IP6s from the string to binary **/
void Initializer::getIP6Converted() {
    if ((inet_pton (AF_INET6, this->srcIp6, &srcIp6Conv)) != 1)
        throw "Initializer::getIP6Converted - Can't convert the source IPv6!";
}

/** Function to get the multicast address **/
void Initializer::getMulticastIP() {

    // Convert the solicited multicast prefix
    uint8_t solicitedPreFix[IP6_ADDRESS_SIZE];
    if ((inet_pton (AF_INET6, IPV6_MULTICAST, &solicitedPreFix)) != 1)
        throw "Initializer::getMulticastIP - Can't convert the multicast IPv6!";

    // Create the final solicited multicast
    memcpy(this->dstMulticastIP, solicitedPreFix, IP6_ADDRESS_SIZE*sizeof(uint8_t));
}

/** Function to get the multicast router address **/
void Initializer::getMulticastRouterIP() {

    // Convert the solicited multicast prefix
    uint8_t solicitedPreFix[IP6_ADDRESS_SIZE];
    if ((inet_pton (AF_INET6, IPV6_MULTICAST_ROUTER, &solicitedPreFix)) != 1)
        throw "Initializer::getMulticastRouterIP - Can't convert the multicast IPv6!";

    // Create the final solicited multicast
    memcpy(this->dstMulticastRouterIP, solicitedPreFix, IP6_ADDRESS_SIZE*sizeof(uint8_t));
}

/** Convert the victim two MAC into binary **/
void Initializer::getSrcMulticastMAC() {

    unsigned int v[6];
    string srcMulticastMACStr = "33:33:00:00:00:01";
    if( 6 == sscanf( srcMulticastMACStr.c_str(), "%x:%x:%x:%x:%x:%x", &v[0], &v[1], &v[2], &v[3], &v[4], &v[5] ) )
        for( int i = 0; i < 6; ++i ) this->srcMulticastMAC[i] = (uint8_t) v[i];
    else throw "Initializer::getSrcMulticastMAC - Can't convert multicast MAC! " + srcMulticastMACStr;
}

/** Get all hosts range **/
void Initializer::getHostsRange() {

    // Get the subnet
    uint32_t subnet = inet_addr(this->srcIp);
    if (subnet == INADDR_NONE) throw "Can't get source IP";
    subnet = ntohl(subnet);

    // Get the mask
    struct in_addr in;
    if (inet_pton(AF_INET, this->srcMask, &in) == 0)
        throw "Arp::getAllHosts - Can't get the mask";

    // Get the mask suffix
    Helpers arpHelpers;
    int maskSuffix = arpHelpers.calcBites(ntohl(in.s_addr));
    if (maskSuffix < 1 || maskSuffix > 32)
        throw "Arp::getAllHosts - Wrong mask suffix!";

    // Get the high and low addresses
    u_int32_t networkMask = 0xffffffff << (32 - maskSuffix);
    this->hostsRange.ip_lo = (htonl(subnet & networkMask));
    this->hostsRange.ip_hi = htonl(subnet | ~networkMask);

    // Get the range number
    uint32_t cur_ip = this->hostsRange.ip_lo;
    int count = 0;
    do { cur_ip = htonl(ntohl(cur_ip) + 1); count++;}
    while (ntohl(cur_ip) <= ntohl(this->hostsRange.ip_hi));
    this->hostsRange.count = count;
}