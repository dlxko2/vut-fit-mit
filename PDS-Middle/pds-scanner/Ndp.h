/**
 * This class implements the NDP functionality
 * @class Ndp
 * @author Martin Pavelka
 * @date 26.02.2017
 */

#ifndef PDS_SCANNER_NDP_H
#define PDS_SCANNER_NDP_H

class Ndp {
public:
    Ndp(std::string interface);
private:
    string interface;
    Initializer ndpInitializer;
    void createPacketPing(bool routers);
    uint16_t calculateChecksum(uint16_t *address, int len);
};

#endif
