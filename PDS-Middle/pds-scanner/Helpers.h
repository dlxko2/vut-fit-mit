/**
 * This class implements helpers functions to cover the minor functions
 * They are used in the whole project and covers network operations
 * @class Helpers
 * @author Martin Pavelka
 * @date 21.02.2017
 */

#include <cstdint>
#include <string>
#include <list>
#include <iostream>
#include <unistd.h>
#include <csignal>
#include <linux/types.h>
#include <arpa/inet.h>
using namespace std;

#ifndef PDS_SCANNER_HELPERS_H
#define PDS_SCANNER_HELPERS_H

#define DEBUG 1
#define PSEUDO_HEADER_SIZE 40
#define ICMP_PING_HEADER_SIZE 16
#define MAXIMUM_PACKET_SIZE 65535
#define IPV6_HEADER_SIZE 40
#define IPV6_HOP_MAX 255
#define IPV6_VERSION 6
#define IPV6_ICMP_NEXT 58
#define PSEUDO_HEADER_INFO 8
#define ZERO 0
#define ARP_HARDWARE_TYPE     1
#define ARP_HW_ADDRESS_LENGHT 6
#define ARP_PR_ADDRESS_LENGHT 4
#define ARP_OPERATION         1
#define MAC_ADDRESS_SIZE      6
#define IP_ADDRESS_SIZE       4
#define ETHERNET_HEADER_SIZE  14
#define ARP_HEADER_SIZE       28
#define MAC_ADDRESS_STR_SIZE  18
#define IP_ADDRESS_STR_SIZE   16
#define CAPTURE_INIT_TIMER    3
#define GET_RESPONDS_TIMER    3
#define IP6_ADDRESS_SIZE      16
#define IPV6_MULTICAST          "ff02::1"
#define IPV6_MULTICAST_ROUTER   "ff02::2"
#define ARP_SEND_WAIT_TIMER  3000000
#define NDP_SEND_WAIT_TIMER  3000000
using namespace std;

class Helpers {
public:
    int calcBites(uint32_t);
};

/** Defines the ARP packet structure **/
struct arpHeader {
    uint16_t htype;
    uint16_t ptype;
    uint8_t hlen;
    uint8_t plen;
    uint16_t opcode;
    uint8_t senderMAC[MAC_ADDRESS_SIZE];
    uint8_t senderIP[IP_ADDRESS_SIZE];
    uint8_t targetMAC[MAC_ADDRESS_SIZE];
    uint8_t targetIP[IP_ADDRESS_SIZE];
};

/** IP addresses structure **/
struct  ipAddressesStruc {
    bool ipv6Predicate;
    string address;
};

/** Structure of list for XML output **/
struct macAddressesStruc{
    string address;
    list<ipAddressesStruc> ipAddresses;
};

struct threadArguments {
    string interface;
    string srcMAC;
};

struct allHostsStruct {
    uint32_t ip_lo;
    uint32_t ip_hi;
    int count;
};




 struct ipv6hdr {

         __u8                    priority:4,
                                 version:4;
         __u8                    flow_lbl[3];
 
         __be16                  payload_len;
         __u8                    nexthdr;
         __u8                    hop_limit;
 
         struct  in6_addr        saddr;
         struct  in6_addr        daddr;
 };

#endif
