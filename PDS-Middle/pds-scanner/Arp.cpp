/**
 * This class implements the ARP functionality
 * @class Arp
 * @author Martin Pavelka
 * @date 21.02.2017
 */

#include "Helpers.h"
#include "Initializer.h"
#include "Arp.h"

/**
 * This is the constructor function
 * @param ifParam - the interface
 */
Arp::Arp(std::string ifParam) : arpInitializer(ifParam) {

    // Initialize class
    this->interface = ifParam;

    // Initialize initializer
    arpInitializer.Initialize4();
    if (arpInitializer.error) {
        cerr << "Arp::Arp - Skipping ARP because of initializer error!" << endl;
        return;
    }

    // Initialize the destination MAC
    this->destinationMAC = (uint8_t *) malloc (MAC_ADDRESS_SIZE * sizeof (uint8_t));
    if (this->destinationMAC == NULL) {
        cerr << "Arp::Arp - Can't allocate the memory!" << endl;
        return;
    }

    // Update the destination MAC for broadcast
    memset (this->destinationMAC, 0xff, MAC_ADDRESS_SIZE * sizeof (uint8_t));

    // Initialize the info for packets
    try {
        this->initializeDevice();
        this->initializeSocket();
        this->initializeEthernet();
        this->initializeArp();
    } catch (const char* msg) {
        cerr <<  msg << endl;
        return;
    };

    // Get all hosts
    try { this->getAllHosts(); }
    catch (const char* msg) {
        cerr << msg << endl;
    };
}

/** The destructor function **/
Arp::~Arp(void) {

    free(this->destinationMAC);
    close(socketArp);
}

/** Initialize the device structure **/
void Arp::initializeDevice() {

    memset(&this->device, ZERO, sizeof(this->device));
    this->device.sll_family = AF_PACKET;
    this->device.sll_halen = ARP_HW_ADDRESS_LENGHT;
    memcpy(this->device.sll_addr, this->arpInitializer.srcMac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    if ((this->device.sll_ifindex = if_nametoindex(this->interface.c_str())) == ZERO)
        throw "Arp::initializeDevice - Can't get index of the device";
}

/** Initialize the socket **/
void Arp::initializeSocket() {

    if ((this->socketArp = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < ZERO)
        throw "Arp::initializeSocket - Can't open the socket for ARP packet!";
}

/** Initialize the ethernet header **/
void Arp::initializeEthernet() {

    memcpy(this->ethernet.h_dest, this->destinationMAC, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    memcpy(this->ethernet.h_source, this->arpInitializer.srcMac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    this->ethernet.h_proto = htons(ETH_P_ARP);
}

/** Initialize the arp packet **/
void Arp::initializeArp() {

    this->arp.htype = htons(ARP_HARDWARE_TYPE);
    this->arp.ptype = htons(ETH_P_IP);
    this->arp.hlen = ARP_HW_ADDRESS_LENGHT;
    this->arp.plen = ARP_PR_ADDRESS_LENGHT;
    this->arp.opcode = htons(ARP_OPERATION);
    memcpy(this->arp.senderMAC, this->arpInitializer.srcMac, MAC_ADDRESS_SIZE * sizeof(uint8_t));
    memcpy(this->arp.senderIP, this->arpInitializer.srcIpConv, IP_ADDRESS_SIZE * sizeof(uint8_t));
    memset(&this->arp.targetMAC, ZERO, MAC_ADDRESS_SIZE * sizeof(uint8_t));
}

/**
 * Function to create the ARP packet for the ip
 * @param ip - the ip to send packet
 */
void Arp::createPacket(uint32_t *ip) {

    // Create the packet
    char *res = (char *) calloc(IP_MAXPACKET, sizeof(char));
    memcpy(this->arp.targetIP, ip, IP_ADDRESS_SIZE);
    memcpy(res, &this->ethernet, ETHERNET_HEADER_SIZE);
    memcpy(res + ETHERNET_HEADER_SIZE, &arp, ARP_HEADER_SIZE * sizeof(uint8_t));

    // Send the packet
    size_t len = ETHERNET_HEADER_SIZE + ARP_HEADER_SIZE;
    if ((sendto(this->socketArp, res, len, ZERO, (struct sockaddr *) &this->device, sizeof(this->device))) <= ZERO)
        throw "Arp::createPacket - Can't send the packet";
}

/** Function to get all hosts in the network **/
void Arp::getAllHosts() {

    // Initialize sleep timer for sending this packets
    struct timespec sleepTimer;
    sleepTimer.tv_sec = 0;
    sleepTimer.tv_nsec = ARP_SEND_WAIT_TIMER;

    // Cycle for all addresses in the local network
    uint32_t cur_ip = this->arpInitializer.hostsRange.ip_lo;
    do {
        // Try to send the packet
        try { createPacket(&cur_ip); }
        catch (const char* msg) {cerr << msg << endl;};

        // Sleep and increment the ip
        nanosleep(&sleepTimer, NULL);
        cur_ip = htonl(ntohl(cur_ip) + 1);
    }
    while (ntohl(cur_ip) <= ntohl(this->arpInitializer.hostsRange.ip_hi));
}





