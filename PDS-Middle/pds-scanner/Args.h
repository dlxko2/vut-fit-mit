/**
 * This class implements the arguments functionality
 * @class Args
 * @author Martin Pavelka
 * @date 22.02.2017
 */

#ifndef PDS_SCANNER_ARGUMENTS_H
#define PDS_SCANNER_ARGUMENTS_H

#include <getopt.h>

class Args {
public:
    Args(int argc, char *argv[]);
    string interface;
    string file;
    string error;
private:
    void help();
};

#endif
