/**
 * This class implements helpers functions to cover the minor functions
 * They are used in the whole project and covers network operations
 * @class Helpers
 * @author Martin Pavelka
 * @date 21.02.2017
 */

#include "Helpers.h"


/**
 * Calculates the positive bites count in the number
 * @see Arp::getAllHosts()
 * @param number - the number to be processed
 * @return - the number of positive bites
 * @author https://github.com/nmav/ipcalc/blob/master/ipcalc.c
 */
int Helpers::calcBites(uint32_t number)
{
    // Initialization
    int counted = 0;
    unsigned int seen_one = 0;

    // While have something to parse
    while (number > 0) {

        // Check for positive
        if (number & 1) {
            seen_one = 1;
            counted++;
        }
        else if (seen_one) return -1;

        // Continue in the number
        number >>= 1;
    }

    // Return the integer number of ones
    return counted;
}
