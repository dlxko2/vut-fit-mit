/**
 * This class implements the initializer functions
 * They are used in the project start to set the important parameters
 * @class Initializer
 * @author Martin Pavelka
 * @date 21.02.2017
 */

#ifndef PDS_SCANNER_INITIALIZER_H
#define PDS_SCANNER_INITIALIZER_H

#include <arpa/inet.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdlib.h>
#include <regex>
#include <cstring>

class Initializer {
public:

    // Functions
    Initializer(string interface);
    ~Initializer(void);
    void Initialize4();
    void Initialize6();

    // Error
    bool error;

    // MAC addresses
    char    *srcMask;
    uint8_t *srcMac;
    char    *srcMulticastMAC;
    string  myMacStr;

    // IP addresses
    char    *srcIp;
    char    *srcIp6;
    uint8_t srcIpConv[IP_ADDRESS_SIZE];
    uint8_t srcIp6Conv[IP6_ADDRESS_SIZE];
    uint8_t *dstMulticastIP[IP6_ADDRESS_SIZE];
    uint8_t *dstMulticastRouterIP[IP6_ADDRESS_SIZE];
    allHostsStruct hostsRange;

private:

    string interface;
    void getMulticastIP();
    void getIPConverted();
    void getIP6Converted();
    void getSourceIP();
    void getSourceMask();
    void getSourceMAC();
    void getSrcMulticastMAC();
    void getSourceIPv6();
    void getMulticastRouterIP();
    void getHostsRange();
};

#endif
