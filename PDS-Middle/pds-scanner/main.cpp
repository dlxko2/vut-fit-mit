/**
 * The main function of pds-scanner
 * Created by Martin Pavelka
 * 16.04.2017
 */

#include "Helpers.h"
#include "Args.h"
#include "Initializer.h"
#include "Capture.h"
#include "Arp.h"
#include "Ndp.h"
#include "XML.h"

pthread_t mainThreadPtr;
Capture mainCapture;
string outputFile;

/**
 * Handle the signals
 * @param signal - the signal number
 */
void sig_handler(int signal) {

    if (signal == SIGINT || signal == SIGUSR1) {
        printf("Received the signal - saving the output!\n");

        // Save the output XML
        list <macAddressesStruc> mainList = mainCapture.getCaptureList();
        XML mainXML(outputFile, mainList);

        // Finish the program
        pthread_cancel(mainThreadPtr);
    }
}

/** Initialize the signals **/
void initializeSignals() {

    if (signal(SIGINT, sig_handler) == SIG_ERR)
        cerr << "[SIGINT]" << "Can't initialize the signals" << endl;
    if (signal(SIGUSR1, sig_handler) == SIG_ERR)
        cerr << "[SIGUSR1]" << "Can't initialize the signals" << endl;
}

/**
 * The main function of program
 * @param argc - the number of arguments
 * @param argv - the array of arguments
 * @return - the integer value
 */
int main(int argc, char *argv[]) {

    // Init the signals
    initializeSignals();

    // Get arguments - wrong argument is not exception
    if (DEBUG) cout << "Parsing the arguments" << endl;
    Args mainArgs(argc, argv);
    if (mainArgs.error.length() > 0) {
        cerr << "[ARGS]" << mainArgs.error << endl;
        return EXIT_FAILURE;
    }

    // Set the output and initialize srcMac
    if (DEBUG) cout << "Getting source MAC address" << endl;
    outputFile = mainArgs.file;
    Initializer srcMacInit(mainArgs.interface);
    if (srcMacInit.error) return EXIT_FAILURE;

    // Create the capturing thread
    if (DEBUG) cout << "Starting capture" << endl;
    mainCapture.initObject(mainArgs.interface, srcMacInit.myMacStr);
    mainThreadPtr = mainCapture.threadPtr;
    sleep(CAPTURE_INIT_TIMER);

    // Process the ARP
    if (DEBUG) cout << "Sending ARP" << endl;
    Arp mainArp(mainArgs.interface.c_str());

    // Process the NDP
    if (DEBUG) cout << "Sending the NDP" << endl;
    Ndp mainNdp(mainArgs.interface.c_str());

    // Wait for the responds
    if (DEBUG) cout <<  "Waiting for responds" << endl;
    sleep(GET_RESPONDS_TIMER);

    // Save the output XML
    if (DEBUG) cout << "Saving the XML" << endl;
    list <macAddressesStruc> mainList = mainCapture.getCaptureList();
    XML mainXML(mainArgs.file, mainList);

    // Finish the program
    if (DEBUG) cout << "Closing" << endl;
    pthread_cancel(mainThreadPtr);
    return EXIT_SUCCESS;
}