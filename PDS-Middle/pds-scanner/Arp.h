/**
 * This class implements the ARP functionality
 * @class Arp
 * @author Martin Pavelka
 * @date 21.02.2017
 */

#ifndef PDS_SCANNER_ARP_H
#define PDS_SCANNER_ARP_H

#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <net/if.h>
#include <cstring>

class Arp {
public:
    Arp(std::string interface);
    ~Arp(void);
    void getAllHosts();
private:
    arpHeader arp;
    ethhdr ethernet;
    int socketArp;
    struct sockaddr_ll device;
    void createPacket(uint32_t *ip);
    uint8_t *destinationMAC;
    Initializer arpInitializer;
    string interface;

    void initializeDevice();

    void initializeSocket();

    void initializeEthernet();

    void initializeArp();
};

#endif
