/**
 * This class implements the XML functionality
 * @class XML
 * @author Martin Pavelka
 * @date 26.02.2017
 */

#ifndef PDS_SCANNER_XML_H
#define PDS_SCANNER_XML_H

#include "Helpers.h"
#include <fstream>

class XML {
public:
    XML(string outputFile, list<macAddressesStruc> captureList);
    ~XML(void);
private:
    void produceXML();
    void openOutputFile();
    ofstream xmlFile;
    string outputFile;
    list<macAddressesStruc> captureList;
};

#endif
