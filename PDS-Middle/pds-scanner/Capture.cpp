/**
 * This class implements the PCAP functionality
 * @class Capture
 * @author Martin Pavelka
 * @date 16.04.2017
 */

#include "Helpers.h"
#include "Capture.h"

/**
 * This is the constructor function
 * @param interface - the name of interface
 */
Capture::Capture() {
}

/**
 * Initialize the instance variables
 * @param interface - the interface
 * @param srcMAC - the source MAC address
 */
void Capture::initObject(string interface, string srcMAC) {
    threadArguments * arguments = new threadArguments();
    arguments->interface = interface;
    arguments->srcMAC = srcMAC;
    pthread_create(&this->threadPtr, NULL, this->captureArp, (void *) arguments);
}

/** This is global list variable*/
list<macAddressesStruc> finalList;

/**
 * This is getter for global list variable
 * @return list of macAddresses struct
 */
list<macAddressesStruc> Capture::getCaptureList() {
    return finalList;
}

/**
 * Function to capture the packages
 * @param arg - interface
 */
 void * Capture::captureArp(void *arg) {

    // Get the interface
    struct threadArguments *arguments = (struct threadArguments*)arg;

    try {
        // Get the device info
        bpf_u_int32 netAddress = 0, mask = 0;
        char errorBuffer[PCAP_ERRBUF_SIZE];
        if (pcap_lookupnet(arguments->interface.c_str(), &netAddress, &mask, errorBuffer) == -1)
            throw "captureArp: Can't get the information about device";

        // Open the device for the capture
        pcap_t *descriptor = NULL;
        if ((descriptor = pcap_open_live(arguments->interface.c_str(), BUFSIZ, 0, 512, errorBuffer)) == NULL)
            throw "captureArp: Can't open the device for the monitoring";

        // Create the filter for ARP
        struct bpf_program filter;
        string filterText = "( arp || ip6[40] == 129 ) && ether dst " + arguments->srcMAC;
        if (pcap_compile(descriptor, &filter, filterText.c_str(), 0, mask) == -1)
            throw "captureArp: Can't create the filter";

        // Set the filter onto device
        if (pcap_setfilter(descriptor, &filter) == -1)
            throw "captureArp: Can't set the filter";

        // Loop the pcap packet delivery
        pcap_loop(descriptor, 0, Capture::my_packet_handler, NULL);
    }
    catch (const char* msg) {cerr << msg << endl;};
    pthread_exit(0);
}

/**
 * Function to handle packet respond functionality
 * @todo we need to check for duplicated IPs too
 * @param args - the arguments
 * @param header - the packet header
 * @param packet - the packet data
 */
void Capture::my_packet_handler(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {

    // Get the ethernet header
    (void)header; (void)args;
    ethhdr *eth = NULL;
    eth = (struct ethhdr *) packet;

    // Initialize the storage variables
    bool process = false;
    bool processIpv6 = false;
    string ipToSave;
    string macToSave;

    // If we have IPv6 packets
    if (eth->h_proto == htons(ETH_P_IPV6)) {

        // Get the ipv6 and convert it into string
        ipv6hdr *ipv6 = NULL;
        char str[INET6_ADDRSTRLEN];
        ipv6 = (struct ipv6hdr *) (packet + ETHERNET_HEADER_SIZE);
        inet_ntop(AF_INET6, &(ipv6->saddr), str, INET6_ADDRSTRLEN);

        // Get the MAC address
        char macStr[MAC_ADDRESS_STR_SIZE];
        snprintf(macStr, MAC_ADDRESS_STR_SIZE, "%02x%02x.%02x%02x.%02x%02x",
                 eth->h_source[0], eth->h_source[1], eth->h_source[2],
                 eth->h_source[3], eth->h_source[4], eth->h_source[5]);

        // Save the ip and mac and set the flags
        ipToSave= string(str);
        macToSave = string(macStr);
        processIpv6 = true;
        process = true;
    }

    // If we have the ARP packet
    else if (eth->h_proto == htons(ETH_P_ARP)){

        // Initialize the ARP header
        arpHeader *arp = NULL;
        arp = (struct arpHeader *) (packet + ETHERNET_HEADER_SIZE);
        if (ntohs(arp->opcode) == 1) return;

        // Save the MAC address
        char macStr[MAC_ADDRESS_STR_SIZE];
        snprintf(macStr, MAC_ADDRESS_STR_SIZE, "%02x%02x.%02x%02x.%02x%02x",
                 arp->senderMAC[0], arp->senderMAC[1], arp->senderMAC[2],
                 arp->senderMAC[3], arp->senderMAC[4], arp->senderMAC[5]);

        // Get the IP address
        char ipStr[IP_ADDRESS_STR_SIZE];
        snprintf(ipStr, IP_ADDRESS_STR_SIZE, "%d.%d.%d.%d", arp->senderIP[0],
                 arp->senderIP[1], arp->senderIP[2], arp->senderIP[3]);

        // Save the ip and the MAC and set the flags
        macToSave = string(macStr);
        ipToSave = string(ipStr);
        process = true;
    }

    // If we don't have any success
    if (!process) return;
    cout << "Mac: " << macToSave << " Ip: " << ipToSave << endl;

    // Create the IP node
    ipAddressesStruc nodeIP;
    nodeIP.ipv6Predicate = processIpv6;
    nodeIP.address = ipToSave;

    // Initialize the duplication flags
    bool macExists = false;
    bool ipExists = false;

    // Browse the list of the MAC addresses
    for(list<macAddressesStruc>::iterator mac = finalList.begin(); mac != finalList.end(); mac++) {

        // If we have this MAC
        if (mac->address.compare(macToSave) == 0) {
            macExists = true;

            // Browse the list of IP addresses and look for duplicated
            for (list<ipAddressesStruc>::iterator ip = mac->ipAddresses.begin(); ip != mac->ipAddresses.end(); ip++)
                if (ip->address.compare(ipToSave) == 0)
                    ipExists = true;

            // If the IP not found push it
            if (!ipExists) mac->ipAddresses.push_front(nodeIP);
        }
    }

    // If we don't have the MAC push the whole MAC node
    if (!macExists) {
        macAddressesStruc nodeMAC;
        nodeMAC.address = macToSave;
        nodeMAC.ipAddresses.push_front(nodeIP);
        finalList.push_front(nodeMAC);
    }
}

